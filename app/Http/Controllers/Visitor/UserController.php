<?php

namespace App\Http\Controllers\Visitor;

use App\User;
use App\Specialty;
use App\Resource;
use App\State;
use App\Municipality;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;


class UserController extends Controller
{
     public function my_account()
     {
          $states = State::all();
          $specialties = Specialty::all();
          $municipalitys = array();
          $months = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];

          $user = User::where('id', Auth::user()->id)->first();

          if ($user->state_id) {
               $municipalitys = Municipality::where('estado_id', $user->state_id)->get();
          }

          return view('visitor.my_account', compact('user', 'specialties', 'states', 'municipalitys', 'months'));
     }

     public function my_account_update(Request $request)
     {
          $user = User::where('id', Auth::user()->id)->first();

          $data = $request->validate([
               'name' => 'required|string|max:255',
               'email' => 'required|string|email|max:255|unique:users,email,'.$user->id,
          ]);

          if($request->filled('password')) {
               $data =  $request->validate([
                    'password' => 'confirmed', 'min:7',
               ]);
               $data['password'] = bcrypt(request('password'));
          }

          $data['status'] = request('status') ? request('status') : 0;
          $data['surnames'] = request('surnames') ;
          $data['specialty_id'] = request('specialty_id') ;
          $data['state_id'] = request('state_id');
          $data['municipality_id'] = request('municipality_id');
          $data['birthdate'] = date("Y-m-d", strtotime(request('day').'-'.request('month').'-'.request('year')));
          $data['cedula'] = request('cedula');



          $user->update( $data );

          return redirect()->route('visitor.dashboard')->with('success', 'Informacón actualizada');
     }
}
