<?php

namespace App\Http\Controllers\Visitor;

use App\Resource;
use App\ResourceGroup;
use App\SeenResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;


class ResourceController extends Controller
{
     public function index($group_id = false)
     {
          if ($group_id) {
               $group = ResourceGroup::where('id', $group_id)->first();
               $resource = Resource::where('resource_group_id', $group_id)->orderBy('order', 'asc')->get();

               return view('visitor.resources.lista', compact('group', 'resource'));
          }else {
               return response(view('errors.404'), 404);
          }
     }

     public function see_resource($resource_id = false)
     {
          if ($resource_id) {
               $this->register_visit_resource($resource_id);
               $resource = Resource::where('id', $resource_id)->first();
               return view('visitor.resources.resource', compact('resource'));
          }else {
               return response(view('errors.404'), 404);
          }
     }

     public function register_visit_resource($resource_id)
     {
          $user_id =  Auth::user()->id;
          $existe = SeenResource::where('user_id', $user_id)->where('resource_id', $resource_id)->first();

          $data['user_id'] = Auth::user()->id;
          $data['resource_id'] = $resource_id;
          if ($existe) {
               $registe = $existe->update( $data );
          }else{
               $registe = SeenResource::create($data);
          }

          return $registe;
     }
}
