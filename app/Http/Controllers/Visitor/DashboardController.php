<?php

namespace App\Http\Controllers\Visitor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ResourceGroup;


class DashboardController extends Controller
{
    public function index()
    {
         $groups = ResourceGroup::orderBy('order', 'asc')->get();
         return view('visitor.dashboard', compact('groups'));
    }
}
