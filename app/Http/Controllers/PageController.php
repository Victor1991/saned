<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use App\Municipality;


class PageController extends Controller
{
     public function home()
     {
          return view('welcome');
     }

     public function municipios(Request $request)
     {
          $estado_id = $request->get('estado_id');
          $data = Municipality::where('estado_id', $estado_id)->get();
          return Response::json($data);


     }
}
