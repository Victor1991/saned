<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Specialty;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
     {
          $admins = User::where('is_admin', '1')->get();

          return view('admin.admin.index', compact('admins'));
     }

     public function create()
     {
          $admin = new User;
          return view('admin.admin.create', compact('admin'));
     }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
     {
          $data = $request->validate([
               'name' => 'required|string|max:255',
               'email' => 'required|string|email|max:255|unique:users',
               'password' => 'required | min:8 | confirmed',
          ]);

          $data['surnames'] = request('surnames') ;
          $data['specialty_id'] = 0;
          $data['is_admin'] = 1;
          $data['password'] = bcrypt(request('password')) ;
          $data['status'] = 1;

          $user = User::create($data);

          return redirect()->route('admin.admin.index')->with('success', 'Administrador registrado');
     }


     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function edit(User $admin)
     {
          return view('admin.admin.edit', compact('admin'));
     }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, User $admin)
     {
          $data = $request->validate([
               'name' => 'required|string|max:255',
               'email' => 'required|string|email|max:255|unique:users,email,'.$admin->id,
          ]);

          if($request->filled('password')) {
               $data =  $request->validate([
                    'password' => 'confirmed', 'min:7',
               ]);
               $data['password'] = bcrypt(request('password')) ;
          }

          $data['specialty_id'] = 0 ;
          $data['surnames'] = request('surnames') ;
          $data['status'] = request('status') ? request('status') : 0;

          $admin->update( $data );

          return redirect()->route('admin.admin.index')->with('success', 'Administrador actualizado');

     }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy(User $admin)
     {
          $admin->delete();
          return back()->with('success', 'Administrador eliminado');

     }

}
