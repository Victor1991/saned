<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ResourceGroup;
use App\Resource;
use App\ResourceType;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;


use Response;




class ResourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($group_id = false)
    {
        if ($group_id) {
             $group = ResourceGroup::where('id', $group_id)->first();
             $resource = Resource::where('resource_group_id', $group_id)->orderBy('order', 'asc')->get();

             return view('admin.resources.index', compact('group', 'resource'));
        }else {
             return response(view('errors.404'), 404);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($group_id)
    {
         $group = ResourceGroup::where('id', $group_id)->first();
         $resource = new Resource;
         $resourcetype = ResourceType::all();
         return view('admin.resources.create', compact('group','resource', 'resourcetype'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $group_id)
    {

         $data = $request->validate([
            'nombre' => 'required|string|max:255',
        ]);

        $data['name'] = request('nombre');
        $data['resource_type_id'] = request('resource_type_id');
        $data['file'] = request('file');
        $data['url'] = request('url');
        $data['author'] = request('author');
        $data['description'] = request('description');
        $data['author_specialty'] = request('author_specialty');
        $data['resource_group_id'] = $group_id;
        $data['user_id'] = Auth::user()->id;


        if ($request->file('input_file') != null) {
             $photo = $request->file('input_file')->store('public');
             $data['file'] = Storage::url($photo);
        }

        $maxValue = Resource::where('resource_group_id', $group_id)->orderBy('order', 'desc')->first();
        $data['order'] = (int)$maxValue->order+1;

        $user = Resource::create($data);

        return redirect()->route('resource.index', ['group_id' => $group_id ])->with('success', 'Recurso registrado');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ModelResources  $modelResources
     * @return \Illuminate\Http\Response
     */
    public function edit( $group_id,  Resource $resource)
    {
         $group = ResourceGroup::where('id', $group_id)->first();
          $resourcetype = ResourceType::all();
          return view('admin.resources.edit', compact('group','resource', 'resourcetype'));
     }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ModelResources  $modelResources
     * @return \Illuminate\Http\Response
     */
    public function update( $group_id, Request $request, Resource $resource )
    {

         $data = $request->validate([
            'nombre' => 'required|string|max:255',
        ]);

        $data['name'] = request('nombre');
        $data['resource_type_id'] = request('resource_type_id');
        $data['url'] = request('url');
        $data['author'] = request('author');
        $data['description'] = request('description');
        $data['author_specialty'] = request('author_specialty');

        if ($request->file('input_file') != null) {
             $photo = $request->file('input_file')->store('public');
             $data['file'] = Storage::url($photo);
        }


        $resource->update($data);

        return redirect()->route('resource.index', ['group_id' => $group_id ])->with('success', 'Recurso actulaizado');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ModelResources  $modelResources
     * @return \Illuminate\Http\Response
     */
    public function destroy($group_id, Resource $resource )
    {
         Resource::where('id',$resource->id)->delete();
         return back()->with('success', 'Recurso eliminado');
    }

     public function activate_resource( Request $request)
     {
          $data['publication_date'] = null;
          if (request('status') == 'true') {
               $data['publication_date'] = date('Y-m-d H:i:s');
          }
          $data['status'] = request('status') == 'true' ? 1 : 0;
          $resource = Resource::whereId(request('resource_id'))->update($data);
          $resource = Resource::where('id', request('resource_id'))->first();
          return Response::json($resource);
     }

     public function order_resource(Request $request)
     {

          $orden_array = explode(",", request('order'));
          $order = 1;
          foreach ($orden_array as $key => $id) {
               $resource = Resource::where('id', $id)->first();
               $save['order'] = $order;
               $resource->update($save);
               $order++;
          }
          return Response::json($resource);
     }

}
