<?php

namespace App\Http\Controllers\Admin;

use App\ResourceGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

use Response;

class ResourceGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

         $data['name'] = request('name');
         $data['color'] = request('color');
          $data['user_id'] = Auth::user()->id;

          if (request('eliminar_imagen_group') == 1) {
               $data['image'] = '';
          }

          if ($request->file('image') != null) {
               $photo = $request->file('image')->store('public');
               $data['image'] = Storage::url($photo);
          }

          if (request('eliminar_img_head') == 1) {
               $data['header_image'] = '';
          }

          if ($request->file('header_image') != null) {
               $photo = $request->file('header_image')->store('public');
               $data['header_image'] = Storage::url($photo);
          }


          $existe = ResourceGroup::where('id', request('group_id'))->first();


          if (!is_null($existe)) {
               $existe->update( $data );
          }else{
               $maxValue = ResourceGroup::orderBy('order', 'desc')->first();
               $data['order'] = (int)$maxValue->order+1;
               $group = ResourceGroup::create($data);
          }
          $ResourceGroup = ResourceGroup::updateOrCreate($data);
          return Response::json($ResourceGroup);

    }


    public function edit($id)
    {
         $group = ResourceGroup::where('id', $id)->first();
         return Response::json($group);
    }

     public function order_group(Request $request)
     {
          $orden_array = explode(",", request('order'));
          $order = 1;
          foreach ($orden_array as $key => $id) {
               $group = ResourceGroup::where('id', $id)->first();
               $save['order'] = $order;
               $group->update($save);
               $order++;
          }
          return Response::json($group);
     }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ $r
     * @return \Illuminate\Http\Response
     */
    public function destroy(ResourceGroup $group)
    {

          $group->delete();
          return redirect()->route('dashboard')->with('success', 'Grupo eliminado');
    }
}
