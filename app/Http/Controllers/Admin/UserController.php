<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Resource;
use App\Specialty;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\State;
use App\Municipality;


class UserController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
     {
          $resource =  Resource::all();
          $users = User::where('is_admin', '0')->get();

          return view('admin.visitors.index', compact('users', 'resource'));
     }

     public function create()
     {
          $user = new User;
          $specialties = Specialty::all();
          $states = State::all();
          $months = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];

          return view('admin.visitors.create', compact('user', 'specialties', 'states', 'months'));
     }

     /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
             $data = $request->validate([
                 'name' => 'required|string|max:255',
                 'email' => 'required|string|email|max:255|unique:users',
                 'password' => 'required | min:8 | confirmed',
            ]);

            $data['status'] = 1;
            $data['surnames'] = request('surnames') ;
            $data['password'] = bcrypt(request('password')) ;
            $data['specialty_id'] = request('specialty_id') ;
            $data['state_id'] = request('state_id');
            $data['municipality_id'] = request('municipality_id');
            $data['cedula'] = request('cedula');
            $data['birthdate'] = date("Y-m-d", strtotime( request('day').'-'. request('month').'-'. request('year')));


            $user = User::create($data);

            return redirect()->route('admin.visitor.index')->with('success', 'Visitante registrado');
        }


        /**
        * Display the specified resource.
        *
        * @param  int  $id
        * @return \Illuminate\Http\Response
        */
        public function edit(User $visitor)
        {
             $specialties = Specialty::all();
             $municipalitys = array();
             $states = State::all();

             if ($visitor->state_id) {
                  $municipalitys = Municipality::where('estado_id', $visitor->state_id)->get();
             }

               $months = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];

             return view('admin.visitors.edit', compact('visitor','specialties', 'states', 'municipalitys', 'months'));
        }

        /**
        * Update the specified resource in storage.
        *
        * @param  \Illuminate\Http\Request  $request
        * @param  int  $id
        * @return \Illuminate\Http\Response
        */
        public function update(Request $request, User $visitor)
        {
             $data = $request->validate([
                  'name' => 'required|string|max:255',
                  'email' => 'required|string|email|max:255|unique:users,email,'.$visitor->id,
             ]);

             if($request->filled('password')) {
                  $data =  $request->validate([
                       'password' => 'confirmed', 'min:7',
                  ]);
                  $data['password'] = bcrypt(request('password')) ;
             }

             $data['status'] = request('status') ? request('status') : 0;
             $data['surnames'] = request('surnames') ;
             $data['specialty_id'] = request('specialty_id') ;
             $data['state_id'] = request('state_id');
             $data['municipality_id'] = request('municipality_id');
             $data['cedula'] = request('cedula');
             $data['birthdate'] = date("Y-m-d", strtotime( request('day').'-'. request('month').'-'. request('year')));


             $visitor->update( $data );

               return redirect()->route('admin.visitor.index')->with('success', 'Visitantes actualizado');

        }

        /**
           * Remove the specified resource from storage.
           *
           * @param  int  $id
           * @return \Illuminate\Http\Response
           */
          public function destroy(Request $request, User $visitor )
          {
               // User::eliminado('id', Auth::user()->id)->first();
               $visitor->delete();
                return back()->with('success', 'Visitantes eliminado');
          }

          public function my_account()
          {
               $user = User::where('id', Auth::user()->id)->first();
               return view('admin.my_account', compact('user'));
          }

          public function my_account_update(Request $request)
          {
               $user = User::where('id', Auth::user()->id)->first();
               $data = $request->validate([
                    'name' => 'required|string|max:255',
                    'email' => 'required|string|email|max:255|unique:users,email,'.$user->id,
               ]);

               if($request->filled('password')) {
                    $data =  $request->validate([
                         'password' => 'confirmed', 'min:7',
                    ]);
                    $data['password'] = bcrypt(request('password')) ;

               }

               $data['surnames'] = request('surnames') ;
               $user->update( $data );

                 return redirect()->route('visitor.dashboard')->with('success', 'Información actualizada');

          }


   }
