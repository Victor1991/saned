<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\ResourceGroup;


class DashboardController extends Controller
{
     public function index()
     {
          $users = User::where('is_admin', '0')->get();
          $admin = User::where('is_admin', '1')->get();
          $groups = ResourceGroup::orderBy('order', 'asc')->get();
          return view('admin.dashboard', compact('users', 'groups', 'admin'));
     }
}
