<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Resource extends Model
{

     protected $fillable = [
          'name',
          'resource_type_id',
          'file',
          'url',
          'author',
          'description',
          'author_specialty',
          'resource_group_id',
          'user_id',
          'order',
     ];

     public function resource_type()
     {
          return $this->belongsTo(ResourceType::class);
     }

     public function registered_user() {
          return $this->hasOne(SeenResource::class)->where('user_id', Auth::user()->id);
     }

     public function resource_group()
     {
          return $this->hasOne(ResourceGroup::class, 'id', 'resource_group_id');
     }

}
