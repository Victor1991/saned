<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use App\Notifications\ResetPasswordNotification;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
     use Notifiable;

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $fillable = [
          'name', 'email', 'password', 'surnames', 'specialty_id', 'last_login_at',
          'last_login_ip', 'status', 'is_admin', 'leido', 'aceptado','state_id' ,'municipality_id', 'birthdate', 'cedula', 'receive_information'
     ];

     /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
     protected $hidden = [
          'password', 'remember_token',
     ];

     /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
     public function sendPasswordResetNotification($token)
     {
          $this->notify(new ResetPasswordNotification($token));
     }

     public function setPasswordAttribute($password)
     {
          $this->attributes['password'] = $password;
     }

     public function specialty()
     {
          return $this->belongsTo(Specialty::class);
     }

     public function resources_seen()
     {
          return $this->hasMany(SeenResource::class);
     }
}
