<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeenResource extends Model
{
     protected $fillable = [
         'user_id', 'resource_id'
    ];
}
