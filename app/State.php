<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
     protected $table = 'estados';

     public function municipality()
     {
          return $this->hasMany(Municipality::class);
     }

}
