<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResourceGroup extends Model
{
     protected $fillable = [
         'name', 'id', 'color', 'image', 'header_image', 'user_id', 'order'
    ];

    public function resources()
    {
        return $this->hasMany(Resource::class);
    }

    public function active_resources()
    {
        return $this->hasMany(Resource::class)->where('status', 1);
    }
}
