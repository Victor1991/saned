@extends('visitor.layout')

@section('content')
@if($group->header_image)
<div class="row-full">
     <img src="{{ $group->header_image  ? $group->header_image : URL::asset('/img/Backgr.jpg')  }}" alt=""  style="width:100%">
</div>
<div class="clearfix">
</div>
<br>
<br>
<br>
@endif

<div class="row">
     <div class="col-xs-6">
          <a href="{{ url('/visitor') }}">
               <h4 style="margin-top:3px;"><span class="label label-success">
                    <i class="fa fa-arrow-left"></i> Atras</span></h4>
          </a>
     </div>
     <div class="col-xs-6" style="text-align: right;">
          <ol class="breadcrumb">
               <li class="breadcrumb-item"><a href="{{ url('/visitor') }}">Inicio</a></li>
               <li class="breadcrumb-item active" aria-current="page">Temas disponibles ->{{ $group->name }} </li>
          </ol>
     </div>
</div>
<div class="row">
     <div class="col-md-12">
          <div class="card gmd-5">
               <div class="card-header">
                    <h4 class="card-title">Temario</h4>
                    <p class="category"> {{ $group->name }} \ Temas disponibles</p>
               </div>
               <div class="card-content table-responsive table-full-width">
                    <table class="table">
                         <thead>
                              <tr>
                                   <th class="text-center">Tema</th>
                                   <th class="text-center">Autor</th>
                                   <th class="text-center">Formato</th>
                                   <th class="text-center"></th>
                              </tr>
                         </thead>
                         <tbody>
                              @forelse ($resource as $resource)

                              <tr>
                                   <td class="text-center">{{ $resource->name }}</td>
                                   <td class="text-center">{{ $resource->author }}</td>
                                   <td class="text-center">{{ $resource->resource_type->name }}</td>
                                   <td class="text-center">
                                        @if($resource->registered_user)
                                             <a  href="{{ route('visitor.see_resource', ['resource_id' => $resource->id ]) }}" type="button" class="btn btn-info btn-sm">Ver de nuevo</a>
                                        @else
                                             <a  href="{{ route('visitor.see_resource', ['resource_id' => $resource->id ]) }}" type="button" class="btn btn-success btn-sm">Ver tema</a>
                                        @endif
                                   </td>
                              </tr>

                              @empty
                              <tr>
                                   <td class="text-center" colspan="5">Sin recursos</td>
                              </tr>
                              @endforelse


                         </tbody>
                    </table>
               </div>
          </div>
     </div>
</div>
@stop

@push('style')
<style media="screen">
.btn-sm{
      border-radius: 5px;
}
.row-full{
width: 100vw;
position: relative;
margin-left: -50vw;
height: 100px;
margin-top: -30px;
margin-button: 20px;
left: 49%;
display: inline-table;
}
.card{
     margin-bottom: 100px;
}
</style>
@endpush
