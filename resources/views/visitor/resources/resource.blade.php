@extends('visitor.layout')

@section('content')
@if($resource->resource_group->header_image)
<div class="row-full">
     <img src="{{ $resource->resource_group->header_image }}" alt=""  style="width:100%">
</div>
<div class="clearfix">
</div>
<br>
<br>
<br>
@endif

<div class="row">
     <div class="col-xs-6">
          <a href="{{ url('/visitor') }}">
               <h4 style="margin-top:3px;"><span class="label label-success">
                    <i class="fa fa-arrow-left"></i> Atras</span></h4>
          </a>
     </div>
     <div class="col-xs-6" style="text-align: right;">
          <ol class="breadcrumb">
               <li class="breadcrumb-item"><a href="{{ url('/visitor') }}">Inicio</a></li>
               <li class="breadcrumb-item"><a href="{{ url('/visitor/resource_group/'. $resource->resource_group_id ) }}">Lista de temas </a></li>
               <li class="breadcrumb-item active" aria-current="page">{{$resource->name}}</li>
          </ol>
     </div>
</div>


<div class="row">
     <div class="col-md-12">
          <div class="card gmd-5">
               <div class="card-content ">
                    <div class="col-md-5">
                         <H2>{{ $resource->name }}</H2>
                         <p>Autor</p>
                         <h5>{{ $resource->author }}</h5>
                         <p>Fecha</p>
                         <h5>{{ \Carbon\Carbon::parse($resource->publication_date)->diffForHumans() }}</h5>
                    </div>

                    <div class="col-md-7 text-center"  style="height:300px; border: 5px solid rgba(12,48,164,0.39); border-radius: 12px 0px 0px 0px; margin-bottom: 15px; height: 280px; background-color:#ddd; overflow-y:auto;">
                         @if($resource->resource_type_id == '1')
                         <iframe src="{{ $resource->file }}" width="100%" height="500px" style="margin: 0 auto;"></iframe>
                         @elseif($resource->resource_type_id == '2')
                         <img src="{{ $resource->file }}" alt="" style="margin-top: 15%; margin: 0 auto;">
                         @elseif($resource->resource_type_id == '3')
                         <video src="{{ $resource->file }}" controls style="width:500px; margin: 0 auto;" >
                         </video>
                         @elseif($resource->resource_type_id == 4)
                         <a href="{{ $resource->url }}" target="_blank" style="color: #000;">
                              <img src="{{URL::asset('/img/link.jpg')}}" alt="Linkweb" style="width:300px; margin-top:20px;">
                              <h3>clic aquí en el enlace</h3>
                         </a>
                         @endif
                    </div>
               </div>
               <div class="col-md-12">
                    <hr>
               </div>
               <div class="col-md-12" style="margin-bottom:20px;">
                    <p>Descripción</p>
                    {{ $resource->description }}
               </div>

               <div class="clearfix"></div>
          </div>
     </div>
</div>


@stop
@push('style')
<style media="screen">
.btn-sm{
     border-radius: 5px;
}
.row-full{
     width: 100vw;
     position: relative;
     margin-left: -50vw;
     height: 100px;
     margin-top: -30px;
     margin-button: 20px;
     left: 49%;
     display: inline-table;
}
.card{
     margin-bottom: 100px;
}

</style>
@endpush

@push('scripts')
<script>

</script>

@endpush
