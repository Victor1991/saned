@extends('visitor.layout')

@section('content')
<div class="row">
     <div class="col-md-12">
          <div class="card">
               <div class="card-header">
                    <h4 class="card-title">
                         Editar participante
                    </h4>
               </div>
               <div class="card-content ">
                    <form class="form" method="post" action="{{ route('visitor.my_account_update', $user) }}">
                         {{ csrf_field() }} {{ method_field('PUT') }}
                         <div class="row">
                              <div class="col-md-12">
                                   @if($errors->any())
                                   <ul class="list-group">
                                        @foreach($errors->all() as $key => $error)
                                        <li class="list-group-item list-group-item-danger"> {{ $error }}</li>
                                        @endforeach
                                   </ul>
                                   @endif
                              </div>
                         </div>
                         <div class="row">
                              <div class="col-md-6">
                                   <div class="form-group">
                                        <label>Nombre</label>
                                        <input type="text" placeholder="Nombre" name="name" class="form-control"  value="{{ $user->name }}">
                                   </div>
                              </div>
                              <div class="col-md-6">
                                   <div class="form-group">
                                        <label>Apellidos</label>
                                        <input type="text" name="surnames" placeholder="Apellidos" class="form-control" value="{{ $user->surnames }}">
                                   </div>
                              </div>
                              <div class="col-md-6">
                                   <div class="form-group">
                                        <label>Especialiedad</label>
                                        <select class="form-control" name="specialty_id">
                                             @foreach($specialties as $key => $specialty)
                                             <option @if( $user->specialty_id == $specialty->id ) checked @endif  value="{{ $specialty->id }}">{{ $specialty->name }}</option>
                                             @endforeach
                                        </select>
                                   </div>
                              </div>
                              <div class="col-md-6">
                                   <div class="form-group">
                                        <label>Correo</label>
                                        <input type="email"  value="{{  $user->email }}" name="email" placeholder="Correo" class="form-control">
                                   </div>
                              </div>
                              <div class="col-md-6">
                                   <div class="form-group">
                                        <label>Estado</label>
                                        <select class="form-control" name="state_id" required id="estado_id" onchange="get_municipios()" >
                                             @foreach($states as $key => $state)
                                             <option
                                                  value="{{ $state->id }}"
                                                  @if( $user->state_id == $state->id ) selected @endif
                                             >{{ $state->nombre }}</option>
                                             @endforeach
                                        </select>

                                        @if ($errors->has('state_id'))
                                        <span class="help-block">
                                             <strong>{{ $errors->first('state_id') }}</strong>
                                        </span>
                                        @endif
                                   </div>
                              </div>
                              <div class="col-md-6">
                                   <div class="form-group">
                                        <label>Municipio / Ciudad </label>

                                        <select class="form-control" name="municipality_id" id="municipio_id" required>
                                             @foreach ($municipalitys as $key => $municipality)
                                                  <option
                                                       value="{{ $municipality->id }}"
                                                       @if($user->municipality_id == $municipality->id) selected @endif
                                                  >{{ $municipality->nombre }}</option>
                                             @endforeach
                                             @if($municipalitys)
                                                  <option
                                                       value="3000"
                                                       @if($user->municipality_id == 3000) selected @endif
                                                  >Otro</option>
                                             @endif
                                        </select>

                                        @if ($errors->has('municipality_id'))
                                        <span class="help-block">
                                             <strong>{{ $errors->first('municipality_id') }}</strong>
                                        </span>
                                        @endif
                                   </div>
                              </div>
                              <div class="col-md-6">
                                   <label for="cedula" >Cédula</label>
                                   <input id="cedula" type="text" class="form-control" name="cedula" value="{{  $user->cedula }}" autofocus>

                                   @if ($errors->has('cedula'))
                                   <span class="help-block">
                                        <strong>{{ $errors->first('cedula') }}</strong>
                                   </span>
                                   @endif

                              </div>
                              <div class="col-md-6">
                                   <label>Fecha de nacimiento</label>
                                   <?php $fecha = strtotime($user->birthdate); ?>
                                   <div class="row">
                                        <div class="col-xs-3">
                                             <select class="form-control" name="day">
                                                  <option hidden value="">Día</option>
                                                  <?php for($j = 0; $j <= 31; $j++){ ?>
                                                       <option
                                                            @if(date('d', $fecha) == $j)
                                                                 selected
                                                            @endif
                                                            value="<?=$j?>"
                                                       ><?=$j?></option>
                                                  <?php } ?>
                                             </select>
                                        </div>
                                        <div class="col-xs-6">
                                             <select class="form-control" name="month">
                                                  <option hidden value="">Mes</option>
                                                  <?php foreach ($months as $key => $month){?>
                                                       <option
                                                            value="<?=$key+1?>"
                                                            @if(date('m', $fecha) == $key+1)
                                                                 selected
                                                            @endif
                                                       ><?=$month?></option>
                                                  <?php } ?>
                                             </select>
                                        </div>
                                        <div class="col-xs-3">
                                             <select class="form-control" name="year">
                                                  <option hidden value="">Año</option>
                                                  <?php for($j = 1920; $j <= date("Y"); $j++){ ?>
                                                       <option
                                                       @if(date('Y', $fecha) == $j)
                                                            selected
                                                       @endif
                                                            value="<?=$j?>"
                                                       ><?=$j?></option>
                                                  <?php } ?>
                                             </select>
                                        </div>
                                   </div>

                                   @if ($errors->has('birthdate'))
                                   <span class="help-block">
                                        <strong>{{ $errors->first('birthdate') }}</strong>
                                   </span>
                                   @endif
                              </div>
                         </div>


                    <div class="">
                         <hr>
                    </div>
                         <div class="row">
                              <div class="col-md-6">
                                   <div class="form-group">
                                        <label>Contraseña</label>
                                        <input type="password" name="password" placeholder="Contraseña" class="form-control">
                                   </div>
                              </div>
                              <div class="col-md-6">
                                   <div class="form-group">
                                        <label>Confirmar Contraseña</label>
                                        <input type="password" name="password_confirmation" placeholder="Confirmar contraseña" class="form-control">
                                   </div>
                              </div>
                         </div>
                         <hr>

                         <a  href="{{ url('/visitor') }}" class="btn btn-fill btn-wd btn-default">
                              <i class="fa fa-arrow-left" aria-hidden="true"></i>
                              Regresar
                         </a>
                         <button type="submit" class="btn btn-fill btn-success btn-wd pull-right">
                              <i class="fa fa-floppy-o" aria-hidden="true"></i>
                              Actualizar información
                         </button>
                    </form>
               </div>
          </div>
     </div>
</div>
@stop
@push('scripts')
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<script>
$( document ).ready(function() {

     $('.datetimepicker').datepicker({
          format: 'dd-mm-yyyy'
     });
});


function get_municipios(){
     $('#municipio_id').html('');
     $.ajax({
          url: "{{ route('municipios') }}",
          data:{'estado_id': $('#estado_id').val()},
          type:'get',
          success:  function (response) {
               $('#municipio_id').html();
               $.each(response, function (i, item) {
                    $('#municipio_id').append($('<option>', {
                         value: item.id,
                         text : item.nombre
                    }));
               });
               $('#municipio_id').append($('<option>', {
                    value: '3000',
                    text : 'Otro'
               }));
          },
          statusCode: {
               404: function() {
                    alert('web not found');
               }
          },
          error:function(x,xs,xt){
               //alert('error: ' + JSON.stringify(x) +"\n error string: "+ xs + "\n error throwed: " + xt);
          }
     });
}
</script>
@endpush

@push('style')
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">

<style>
span.badge.badge-secondary{
     width: 100%;
    padding: 10px;
    background-color: #f3f2ee;
    color: #6a615b;
    border-radius: 5px;
    border: 1px solid #ddd;
}

</style>
@endpush
