@extends('visitor.layout')

@section('content')
<div class="row">
     @foreach ($groups as $key => $group)
     <div class="col-lg-4 col-sm-6">
          <div class="card"
               @if($group->color)
                    style="
                    -webkit-box-shadow: 2px 0px 10px 3px {{ $group->color }};
                    -moz-box-shadow: 2px 0px 10px 3px {{ $group->color }};
                    box-shadow: 2px 0px 10px 3px {{ $group->color }};
                    "
               @endif
               >
               <div class="card-header">
                    <h4 class="card-title">{{ $group->name }}</h4>
               </div>
               <br>
               <div class="card-content">
                    <?php if ( $group->image): ?>
                         <img class="img-height" src="{{ $group->image  }}" alt="">
                    <?php else: ?>
                         <img class="img-height" src="{{  asset('img/img_defe.jpg') }}" alt="">
                    <?php endif; ?>
               </div>
               <br>
               <div class="card-footer">
                    <a
                         type="button"
                         class="btn btn-block btn-lg  btn-info"
                         name="button"
                         href="{{ route('visitor.resource_group', ['group_id' => $group->id ]) }}"
                    >  <i class="fa fa-eye" aria-hidden="true"></i> Ver curso</a>
               </div>
          </div>
     </div>
     @endforeach
</div>
@stop
@push('style')
<style>
img.img-height{
    height: 250px !important;
}
.card-content{
     text-align: center;
     background: #d6d6d6;
     padding: 0px !important;
}

</style>
@endpush
