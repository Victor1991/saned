<html lang="en" class="perfect-scrollbar-off" data-lt-installed="true"><head>
     <meta charset="utf-8">
     <link rel="apple-touch-icon" sizes="76x76" href="{!! asset('theme/img/apple-icon.png') !!}">
     <link rel="icon" type="image/png" sizes="96x96" href="{!! asset('theme/img/favicon.png') !!}">
     <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

     <title>BESINS HEALTHCARE</title>

     <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">
     <meta name="viewport" content="width=device-width">


     <!-- Bootstrap core CSS     -->
     <link href="{!! asset('theme/css/bootstrap.min.css') !!}" rel="stylesheet">

     <!--  Paper Dashboard core CSS    -->
     <link href="{!! asset('theme/css/paper-dashboard.css') !!}" rel="stylesheet">


     <!--  CSS for Demo Purpose, don't include it in your project     -->
     <link href="{!! asset('theme/css/demo.css') !!}" rel="stylesheet">


     <!--  Fonts and icons     -->
     <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
     <link href="https://fonts.googleapis.com/css?family=Muli:400,300" rel="stylesheet" type="text/css">
     <link href="{!! asset('theme/css/themify-icons.css') !!}" rel="stylesheet">

     @stack('style')


     <style>

     .content {
          background-image: url({{URL::asset('/img/3345254.jpg')}});
          background-repeat: no-repeat;
          background-position: center;
          background-size: cover;
          width: 100%;
          height: 100%;
     }


     </style>

     <body>

     <div class="wrapper wrapper-full-page" >
          <div class="full-page login-page" data-color="" >
               <!--   you can change the color of the filter page using: data-color="blue | azure | green | orange | red | purple" -->
               <div class="content">
                    <div class="container">
                         <div class="row">

                              @yield('content')

                         </div>
                    </div>
               </div>

               <footer class="footer footer-transparent">
                    <div class="container">


                          <div class="copyright pull-right">
    &copy; <script>document.write(new Date().getFullYear())</script> -   Desarrollado por Grupo SANED | <a onclick='ver_modal()' style="color: #ffffff">Política de privacidad y Condiciones de uso</a>
     | <a onclick='ver_modal_laboratorio()' style="color: #ffffff">Política de privacidad y Condiciones de uso del laboratorio</a>
    </div>


                    </div>
               </footer>
               <div class="full-page-background" style="background-image: url('{!! asset('theme/background/31799.jpg') !!} "></div></div>
          </div>

          @include('partials.modal-politicas')
          @include('partials.modal-politicas-laboratorio')


          <!--   Core JS Files. Extra: TouchPunch for touch library inside jquery-ui.min.js') !!}   -->
          <script src="{!! asset('theme/js/jquery.min.js') !!}" type="text/javascript"></script>
          <script src="{!! asset('theme/js/jquery-ui.min.js') !!}" type="text/javascript"></script>
          <script src="{!! asset('theme/js/perfect-scrollbar.min.js') !!}" type="text/javascript"></script>
          <script src="{!! asset('theme/js/bootstrap.min.js') !!}" type="text/javascript"></script>

          <!--  Forms Validations Plugin -->
          <script src="{!! asset('theme/js/jquery.validate.min.js') !!}"></script>

          <!-- Promise Library for SweetAlert2 working on IE -->
          <script src="{!! asset('theme/js/es6-promise-auto.min.js') !!}"></script>

          <!--  Plugin for Date Time Picker and Full Calendar Plugin-->
          <script src="{!! asset('theme/js/moment.min.js') !!}"></script>

          <!--  Date Time Picker Plugin is included in this js file -->
          <script src="{!! asset('theme/js/bootstrap-datetimepicker.js') !!}"></script>

          <!--  Select Picker Plugin -->
          <script src="{!! asset('theme/js/bootstrap-selectpicker.js') !!}"></script>

          <!--  Switch and Tags Input Plugins -->
          <script src="{!! asset('theme/js/bootstrap-switch-tags.js') !!}"></script>

          <!-- Circle Percentage-chart -->
          <script src="{!! asset('theme/js/jquery.easypiechart.min.js') !!}"></script>

          <!--  Charts Plugin -->
          <script src="{!! asset('theme/js/chartist.min.js') !!}"></script>

          <!--  Notifications Plugin    -->
          <script src="{!! asset('theme/js/bootstrap-notify.js') !!}"></script>

          <!-- Sweet Alert 2 plugin -->
          <script src="{!! asset('theme/js/sweetalert2.js') !!}"></script>

          <!-- Vector Map plugin -->
          <script src="{!! asset('theme/js/jquery-jvectormap.js') !!}"></script>

          <!--  Google Maps Plugin    -->
          <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAFPQibxeDaLIUHsC6_KqDdFaUdhrbhZ3M"></script>

          <!-- Wizard Plugin    -->
          <script src="{!! asset('theme/js/jquery.bootstrap.wizard.min.js') !!}"></script>

          <!--  Bootstrap Table Plugin    -->
          <script src="{!! asset('theme/js/bootstrap-table.js') !!}"></script>

          <!--  Plugin for DataTables.net  -->
          <script src="{!! asset('theme/js/jquery.datatables.js') !!}"></script>

          <!--  Full Calendar Plugin    -->
          <script src="{!! asset('theme/js/fullcalendar.min.js') !!}"></script>

          <!-- Paper Dashboard PRO Core javascript and methods for Demo purpose -->
          <script src="{!! asset('theme/js/paper-dashboard.js') !!}"></script>

          <!--   Sharrre Library    -->
          <script src="{!! asset('theme/js/jquery.sharrre.js') !!}"></script>

          <!-- Paper Dashboard PRO DEMO methods, don't include it in your project! -->
          <script src="{!! asset('theme/js/demo.js') !!}"></script>



          <script type="text/javascript">
          $().ready(function(){
               demo.checkFullPageBackgroundImage();

               setTimeout(function(){
                    // after 1000 ms we add the class animated to the login/register card
                    $('.card').removeClass('card-hidden');
               }, 700)
          });
          </script>

          @stack('scripts')


          <style>

          #myImg {
               border-radius: 5px;
               cursor: pointer;
               transition: 0.3s;
          }

          #myImg:hover {opacity: 0.7;}

          /* The Modal (background) */
          .modal {
               display: none; /* Hidden by default */
               position: fixed; /* Stay in place */
               z-index: 10; /* Sit on top */
               padding-top: 100px; /* Location of the box */
               left: 0;
               top: 0;
               width: 100%; /* Full width */
               height: 100%; /* Full height */
               overflow: auto; /* Enable scroll if needed */
               background-color: rgb(0,0,0); /* Fallback color */
               background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
          }

          /* Modal Content (Image) */
          .modal-content {
               margin: auto;
               display: block;
               width: 80%;
               max-width: 700px;
          }

          /* Caption of Modal Image (Image Text) - Same Width as the Image */
          #caption {
               margin: auto;
               display: block;
               width: 80%;
               max-width: 700px;
               text-align: center;
               color: #ccc;
               padding: 10px 0;
               height: 150px;
          }

          /* Add Animation - Zoom in the Modal */
          .modal-content, #caption {
               animation-name: zoom;
               animation-duration: 0.6s;
          }

          @keyframes zoom {
               from {transform:scale(0)}
               to {transform:scale(1)}
          }

          /* The Close Button */
          .close {
               position: absolute;
               top: 35px;
               right: 35px;
               color: #f1f1f1;
               font-size: 40px;
               font-weight: bold;
               transition: 0.3s;
          }

          .close:hover,
          .close:focus {
               color: #bbb;
               text-decoration: none;
               cursor: pointer;
          }

          /* 100% Image Width on Smaller Screens */
          @media only screen and (max-width: 700px){
               .modal-content {
                    width: 100%;
               }
          }

          .back{
               background-image: url("https://mdn.mozillademos.org/files/6457/mdn_logo_only_color.png");

          }


          #ofBar {
               background: #de2e2e;
               text-align: left;
               min-height: 60px;
               z-index: 999999999;
               font-size: 16px;
               color: #fff;
               padding: 18px 5%;
               font-weight: 400;
               display: block;
               position: relative;
               top: 0px;
               width: 100%;
               box-shadow: 0 6px 13px -4px rgba(0, 0, 0, 0.25);
          }
          #ofBar b {
               font-size: 15px !important;
          }
          #count-down {
               display: initial;
               padding-left: 10px;
               font-weight: bold;
          }
          #close-bar {
               font-size: 22px;
               color: #3e3947;
               margin-right: 0;
               position: absolute;
               right: 5%;
               background: white;
               opacity: 0.5;
               padding: 0px;
               height: 25px;
               line-height: 21px;
               width: 25px;
               border-radius: 50%;
               text-align: center;
               top: 18px;
               cursor: pointer;
               z-index: 9999999999;
               font-weight: 200;
          }
          #close-bar:hover{
               opacity: 1;
          }
          #btn-bar {
               background-color: #fff;
               color: #40312d;
               border-radius: 4px;
               padding: 10px 20px;
               font-weight: bold;
               text-transform: uppercase;
               font-size: 12px;
               opacity: .95;
               margin-left: 15px;
               top: 0px;
               position: relative;
               cursor: pointer;
               text-align: center;
               box-shadow: 0 5px 10px -3px rgba(0,0,0,.23), 0 6px 10px -5px rgba(0,0,0,.25);
          }
          #btn-bar:hover{
               opacity: 0.9;
          }
          #btn-bar{
               opacity: 1;
          }

          #btn-bar span {
               color: red;
          }
          .right-side{
               float: right;
               margin-right: 60px;
               top: -6px;
               position: relative;
               display: block;
          }

          #oldPriceBar {
               text-decoration: line-through;
               font-size: 16px;
               color: #fff;
               font-weight: 400;
               top: 2px;
               position: relative;
          }
          #newPrice{
               color: #fff;
               font-size: 19px;
               font-weight: 700;
               top: 2px;
               position: relative;
               margin-left: 7px;
          }

          #fromText {
               font-size: 15px;
               color: #fff;
               font-weight: 400;
               margin-right: 3px;
               top: 0px;
               position: relative;
          }

          @media(max-width: 991px){
               .right-side{
                    float:none;
                    margin-right: 0px;
                    margin-top: 5px;
                    top: 0px
               }
               #ofBar {
                    padding: 50px 20px 20px;
                    text-align: center;
               }
               #btn-bar{
                    display: block;
                    margin-top: 10px;
                    margin-left: 0;
               }
          }
          @media (max-width: 768px) {
               #count-down {
                    display: block;
                    font-size: 25px;
               }
          }
          </style>
          <div class="collapse navbar-collapse off-canvas-sidebar"><ul class="nav nav-mobile-menu">
               <li>
                    <a href="register.html">
                         Register
                    </a>
               </li>
               <li>
                    <a href="../dashboard/overview.html">
                         Dashboard
                    </a>
               </li>
          </ul>
     </div>
</body>
</html>
