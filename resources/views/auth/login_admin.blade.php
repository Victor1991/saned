@extends('auth.layout')

@section('content')
<div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
     <form method="POST" action="{{ route('admin.login') }}">
          {{ csrf_field() }}

          <div class="card" data-background="color" data-color="blue" style="background: rgba(0, 10, 0, 0.4); color:#fff; box-shadow:2px 0px 9px 9px #efe4e42b;">
               <div class="card-header text-center">
                    <h3 class="card-title" style="color:#fff;">Bienvenid@</h3>
               </div>
               <div class="card-content">

                    @include('partials.flash-message')


                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                         <label>Correo</label>
                         <input name="email" type="email" placeholder="Correo" class="form-control input-no-border" required autofocus>
                         @if ($errors->has('email'))
                         <span class="help-block">
                              <strong>{{ $errors->first('email') }}</strong>
                         </span>
                         @endif
                    </div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                         <label>Password</label>
                         <input name="password" type="password" placeholder="Password" class="form-control input-no-border" required>
                         @if ($errors->has('password'))
                         <span class="help-block">
                              <strong>{{ $errors->first('password') }}</strong>
                         </span>
                         @endif
                    </div>
                    <!-- <div class="form-group">
                         <div class="col-md-12 ">
                              <div class="checkbox">
                                   <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Recuerdame
                                   </label>
                              </div>
                         </div>
                    </div> -->

               </div>
               <div class="card-footer text-center">
                    <button type="submit" class="btn btn-fill btn-wd ">Ingresar</button>
                    <br>
                    o
                    <br>
                    <div class="forgot">
                         <a href="{{ route('password.request') }}">Olvide contraseña</a>
                    </div>
               </div>
          </div>
     </form>
</div>

@stop
