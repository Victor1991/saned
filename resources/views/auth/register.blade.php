@extends('auth.layout')

@section('content')


<style type="text/css">
     label {
          font-weight: 100;
     }
     .content{
         padding-top: 20vh;
         padding-bottom: 145vh;
     }
</style>

<div class="container">
     <div class="row">
          <div class="col-md-8 col-md-offset-2">
               <div class="panel panel-default" style="background-color: #ffffff color:#385174; box-shadow:2px 0px 9px 9px #efe4e42b;">

                     <div class="card-header text-center">
                     <img src="{{URL::asset('/img/logo.png')}}" alt="SANED" style="width:150px;">
               </div>
                    <div class="panel-body">
                         <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                              {{ csrf_field() }}
                              <input type="hidden" name="status" value="1">

                              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                   <label for="name" class="col-md-4 control-label">Nombre</label>

                                   <div class="col-md-6">
                                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                        @if ($errors->has('name'))
                                        <span class="help-block">
                                             <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                        @endif
                                   </div>
                              </div>

                              <div class="form-group{{ $errors->has('surnames') ? ' has-error' : '' }}">
                                   <label for="name" class="col-md-4 control-label">Apellidos</label>

                                   <div class="col-md-6">
                                        <input id="surnames" type="text" class="form-control" name="surnames" value="{{ old('surnames') }}" required autofocus>

                                        @if ($errors->has('surnames'))
                                        <span class="help-block">
                                             <strong>{{ $errors->first('surnames') }}</strong>
                                        </span>
                                        @endif
                                   </div>
                              </div>


                              <div class="form-group{{ $errors->has('specialty_id') ? ' has-error' : '' }}">
                                   <label for="name" class="col-md-4 control-label">Especialidad</label>

                                   <div class="col-md-6">
                                        <select class="form-control" name="specialty_id">
                                             @foreach($specialties as $key => $specialty)
                                             <option value="{{ $specialty->id }}">{{ $specialty->name }}</option>
                                             @endforeach
                                        </select>

                                        @if ($errors->has('specialty_id'))
                                        <span class="help-block">
                                             <strong>{{ $errors->first('specialty_id') }}</strong>
                                        </span>
                                        @endif
                                   </div>
                              </div>

                              <div class="form-group{{ $errors->has('cedula') ? ' has-error' : '' }}">
                                   <label for="name" class="col-md-4 control-label">Cédula</label>

                                   <div class="col-md-6">
                                        <input id="cedula" type="text" class="form-control" name="cedula" value="{{ old('cedula') }}" autofocus>

                                        @if ($errors->has('cedula'))
                                        <span class="help-block">
                                             <strong>{{ $errors->first('cedula') }}</strong>
                                        </span>
                                        @endif
                                   </div>
                              </div>

                              <div class="form-group{{ $errors->has('state_id') ? ' has-error' : '' }}">
                                   <label for="name" class="col-md-4 control-label">Estado</label>

                                   <div class="col-md-6">
                                        <select class="form-control" name="state_id" required id="estado_id" onchange="get_municipios()" >
                                             @foreach($states as $key => $state)
                                             <option value="{{ $state->id }}">{{ $state->nombre }}</option>
                                             @endforeach
                                        </select>

                                        @if ($errors->has('state_id'))
                                        <span class="help-block">
                                             <strong>{{ $errors->first('state_id') }}</strong>
                                        </span>
                                        @endif
                                   </div>
                              </div>

                              <div class="form-group{{ $errors->has('municipality_id') ? ' has-error' : '' }}">
                                   <label for="municipality_id" class="col-md-4 control-label">Municipio / Ciudad </label>

                                   <div class="col-md-6">
                                        <select class="form-control" name="municipality_id" id="municipio_id" required>

                                        </select>

                                        @if ($errors->has('municipality_id'))
                                        <span class="help-block">
                                             <strong>{{ $errors->first('municipality_id') }}</strong>
                                        </span>
                                        @endif
                                   </div>
                              </div>

                              <div class="form-group{{ $errors->has('birthdate') ? ' has-error' : '' }}">
                                   <label for="name" class="col-md-4 control-label"> Fecha de nacimiento </label>

                                   <div class="col-md-6">
                                        <div class="row">
                                             <div class="col-xs-3">
                                                  <select class="form-control" name="day">
                                                       <option hidden value="">Día</option>
                                                       <?php for($j = 0; $j <= 31; $j++){ ?>
                                                            <option value="<?=$j?>"><?=$j?></option>
                                                       <?php } ?>
                                                  </select>
                                             </div>
                                             <div class="col-xs-6">
                                                  <select class="form-control" name="month">
                                                       <option hidden value="">Mes</option>
                                                       <?php foreach ($months as $key => $month){?>
                                                            <option value="<?=$key+1?>"><?=$month?></option>
                                                       <?php } ?>
                                                  </select>
                                             </div>
                                             <div class="col-xs-3">
                                                  <select class="form-control" name="year">
                                                       <option hidden value="">Año</option>
                                                       <?php for($j = 1920; $j <= date("Y"); $j++){ ?>
                                                            <option value="<?=$j?>"><?=$j?></option>
                                                       <?php } ?>
                                                  </select>
                                             </div>
                                        </div>

                                        @if ($errors->has('birthdate'))
                                        <span class="help-block">
                                             <strong>{{ $errors->first('birthdate') }}</strong>
                                        </span>
                                        @endif
                                   </div>
                              </div>

                              <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                   <label for="email" class="col-md-4 control-label">Correo</label>

                                   <div class="col-md-6">
                                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                        @if ($errors->has('email'))
                                        <span class="help-block">
                                             <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                        @endif
                                   </div>
                              </div>

                              <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                   <label for="password" class="col-md-4 control-label">Password</label>

                                   <div class="col-md-6">
                                        <input id="password" type="password" class="form-control" name="password" required>

                                        @if ($errors->has('password'))
                                        <span class="help-block">
                                             <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                        @endif
                                   </div>
                              </div>

                              <div class="form-group">
                                   <label for="password-confirm" class="col-md-4 control-label">Confirmar Password</label>

                                   <div class="col-md-6">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                   </div>
                              </div>
                              <hr>
                              <div class="form-group">
                                   <div class="col-md-12 col-md-offset-3">
                                        <input type="checkbox" name="leido" value="1" required>
                                        <label style="font-size:15px;">
                                              He leído y acepto la <a  onclick='ver_modal_laboratorio()' >política de privacidad de Saned</a>
                                        </label>
                                   </div>
                                   <div class="col-md-12 col-md-offset-3">
                                        <input type="checkbox" name="aceptado" value="1" required>
                                        <label style="font-size:15px;">
                                             He leído y acepto la <a onclick='ver_modal()'> política de privacidad de Besins</a>
                                        </label>
                                   </div>
                                   <!-- <div class="col-md-7 col-md-offset-3" style="display: inline-flex;">
                                        <input type="checkbox" name="receive_information" value="1" >
                                        <label style="font-size:15px;margin-left: 5px;">
                                             Deseo recibir información sobre productos y servicios de interés para profesionales sanitarios de Besins como: actualidad, formación y comunicaciones comerciales.
                                        </label>
                                   </div> -->
                              </div>


                              <div class="form-group">
                                   <div class="col-md-6 col-md-offset-3">
                                        <button type="submit" class="btn btn-lg btn-info btn-block">
                                             Registrar
                                        </button>
                                   </div>
                              </div>

                         </form>
                    </div>
               </div>
          </div>
     </div>
</div>

@include('partials.modal-politicas')

@endsection

@push('style')
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
@endpush


@push('scripts')
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>

<script>
$( document ).ready(function() {
     get_municipios();

     $('.datetimepicker').datepicker({
          format: 'dd-mm-yyyy'
     });
});


function get_municipios(){
     $('#municipio_id').html('');
     $.ajax({
          url: "{{ route('municipios') }}",
          data:{'estado_id': $('#estado_id').val()},
          type:'get',
          success:  function (response) {
               $('#municipio_id').html();
               $.each(response, function (i, item) {
                    $('#municipio_id').append($('<option>', {
                         value: item.id,
                         text : item.nombre
                    }));
               });
               $('#municipio_id').append($('<option>', {
                    value: '3000',
                    text : 'Otro'
               }));
          },
          statusCode: {
               404: function() {
                    alert('web not found');
               }
          },
          error:function(x,xs,xt){
               //alert('error: ' + JSON.stringify(x) +"\n error string: "+ xs + "\n error throwed: " + xt);
          }
     });
}

</script>
@endpush
