
<div id="myModal_laboratorio" class="modal">

     <!-- The Close Button -->
     <span class="close" id="btn_closed">&times;</span>

     <!-- Modal Content (The Image) -->
     <img class="modal-content" id="img01" src="{!! asset('img/besins-healthcare.jpg') !!}">

     <!-- Modal Caption (Image Text) -->
     <div id="caption"></div>
</div>

@push('scripts')
<script>
// Get the modal
var myModal_laboratorio = document.getElementById("myModal_laboratorio");

// Get the <span> element that closes the modal
var span = document.getElementById("btn_closed");

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  myModal_laboratorio.style.display = "none";
}

function ver_modal_laboratorio() {
     $("#myModal_laboratorio").css("display","block");
}
</script>
@endpush

@push('style')

<style>
#myImg {
     border-radius: 5px;
     cursor: pointer;
     transition: 0.3s;
}

#myImg:hover {opacity: 0.7;}

/* The Modal (background) */
.modal {
     display: none; /* Hidden by default */
     position: fixed; /* Stay in place */
     z-index: 10; /* Sit on top */
     padding-top: 100px; /* Location of the box */
     left: 0;
     top: 0;
     width: 100%; /* Full width */
     height: 100%; /* Full height */
     overflow: auto; /* Enable scroll if needed */
     background-color: rgb(0,0,0); /* Fallback color */
     background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content (Image) */
.modal-content {
     margin: auto;
     display: block;
     width: 80%;
     max-width: 700px;
}

/* Caption of Modal Image (Image Text) - Same Width as the Image */
#caption {
     margin: auto;
     display: block;
     width: 80%;
     max-width: 700px;
     text-align: center;
     color: #ccc;
     padding: 10px 0;
     height: 150px;
}

/* Add Animation - Zoom in the Modal */
.modal-content, #caption {
     animation-name: zoom;
     animation-duration: 0.6s;
}

@keyframes zoom {
     from {transform:scale(0)}
     to {transform:scale(1)}
}

/* The Close Button */
.close {
     position: absolute;
     top: 15px;
     right: 35px;
     color: #f1f1f1;
     font-size: 40px;
     font-weight: bold;
     transition: 0.3s;
}

.close:hover,
.close:focus {
     color: #bbb;
     text-decoration: none;
     cursor: pointer;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
     .modal-content {
          width: 100%;
     }
}
</style>
@endpush
