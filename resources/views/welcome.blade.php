<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
     <meta charset="utf-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1">

     <title>BESINS HEALTHCARE</title>

     <!-- Fonts -->
     <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

     <!-- Bootstrap core CSS     -->
     <link href="{!! asset('theme/css/bootstrap.min.css') !!}" rel="stylesheet">

     <!-- Styles -->
     <link href="{!! asset('theme/css/demo.css') !!}" rel="stylesheet" />

     @yield('style')

     <style>
     #myImg {
          border-radius: 5px;
          cursor: pointer;
          transition: 0.3s;
     }

     #myImg:hover {opacity: 0.7;}

     /* The Modal (background) */
     .modal {
          display: none; /* Hidden by default */
          position: fixed; /* Stay in place */
          z-index: 10; /* Sit on top */
          padding-top: 100px; /* Location of the box */
          left: 0;
          top: 0;
          width: 100%; /* Full width */
          height: 100%; /* Full height */
          overflow: auto; /* Enable scroll if needed */
          background-color: rgb(0,0,0); /* Fallback color */
          background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
     }

     /* Modal Content (Image) */
     .modal-content {
          margin: auto;
          display: block;
          width: 80%;
          max-width: 700px;
     }

     /* Caption of Modal Image (Image Text) - Same Width as the Image */
     #caption {
          margin: auto;
          display: block;
          width: 80%;
          max-width: 700px;
          text-align: center;
          color: #ccc;
          padding: 10px 0;
          height: 150px;
     }

     /* Add Animation - Zoom in the Modal */
     .modal-content, #caption {
          animation-name: zoom;
          animation-duration: 0.6s;
     }

     @keyframes zoom {
          from {transform:scale(0)}
          to {transform:scale(1)}
     }

     /* The Close Button */
     .close {
          position: absolute;
          top: 15px;
          right: 35px;
          color: #f1f1f1;
          font-size: 40px;
          font-weight: bold;
          transition: 0.3s;
     }

     .close:hover,
     .close:focus {
          color: #bbb;
          text-decoration: none;
          cursor: pointer;
     }

     /* 100% Image Width on Smaller Screens */
     @media only screen and (max-width: 700px){
          .modal-content {
               width: 100%;
          }
     }
     body {

          background-image: url({{URL::asset('/img/3345254.jpg')}});
          background-repeat: no-repeat;
          background-position: center;
          background-size: cover;
          width: 100%;
          height: 100%;

     }

     .full-height {
          height: 100vh;
     }

     .flex-center {
          align-items: center;
          display: flex;
          justify-content: center;
     }

     .position-ref {
          position: relative;
     }

     .top-right {
          position: absolute;
          right: 10px;
          top: 18px;
     }

     .content {
          text-align: center;
     }

     .title {
          font-size: 54px;
     }


     .links > a {
          color: #636b6f;
          padding: 0 25px;
          font-size: 12px;
          font-weight: 600;
          letter-spacing: .1rem;
          text-decoration: none;
          text-transform: uppercase;
     }

     .m-b-md {
          margin-bottom: 30px;
     }

     .link_boton{
          color: #ffffff !important ;
          background-color: #002e605c;
          padding: 10px !important;
          border-radius: 5px !important;
          border: 1px solid #ddd;
     }
     body{
          margin-bottom: -1px;
     }
     </style>

</head>
<body>


     <div class="flex-center position-ref full-height main-panel">
          

          <div class="content">
               <div class="title m-b-md">
                    <img src="{{URL::asset('/img/logo.png')}}" alt="SANED" style="width:220px;">
                    <div class="links">
                         @auth
                         <a class="link_boton btn btn-info" href="{{ url('/visitor') }}">Sistema</a>
                         @else
                         <a  class="btn btn-link link_boton" href="{{ route('login') }}">Iniciar sesión</a>

                         @endauth


                    </div>
               </div>

          </div>

     </div>
     <div class="row" style="
     margin-top: 19px;
     background-color: #5998b5;
     margin-left: 0px;
     margin-right: 0px;
     padding: 10px;
     text-align: end;">

     <div class="copyright pull-right">
          &copy; <script>document.write(new Date().getFullYear())</script> -   Desarrollado por Grupo SANED | <a onclick='ver_modal()' style="color: #ffffff">Política de privacidad y Condiciones de uso</a>
          | <a onclick='ver_modal_laboratorio()' style="color: #ffffff">Política de privacidad y Condiciones de uso del laboratorio</a>
     </div>

</div>

@include('partials.modal-politicas')
@include('partials.modal-politicas-laboratorio')


<!--   Core JS Files. Extra: TouchPunch for touch library inside jquery-ui.min.js') !!}   -->
<script src="{!! asset('theme/js/jquery.min.js') !!}" type="text/javascript"></script>
<script src="{!! asset('theme/js/jquery-ui.min.js') !!}" type="text/javascript"></script>
<script src="{!! asset('theme/js/perfect-scrollbar.min.js') !!}" type="text/javascript"></script>
<script src="{!! asset('theme/js/bootstrap.min.js') !!}" type="text/javascript"></script>


@stack('scripts')


</body>
</html>
