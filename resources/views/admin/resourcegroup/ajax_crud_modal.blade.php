<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/ui-lightness/jquery-ui.css">

<div class="modal fade" id="ajax-crud-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
     <div class="modal-dialog" role="document">
          <div class="modal-content">
               <div class="modal-header">
                    <h4 class="modal-title" id="groupCrudModal"></h4>
               </div>
               <form id="groupForm" name="groupForm" enctype="multipart/form-data">
                    <input type="hidden" name="group_id" id="group_id" value="">
                    <div class="modal-body">
                         <div class="row">
                              <div class="col-md-6">
                                   <div class="form-group">
                                        <label>Nombre</label>
                                        <input name="name" id="name" type="text" placeholder="Nombre" class="form-control" maxlength="50" required="">
                                   </div>
                              </div>
                              <div class="col-md-6">
                                   <div class="form-group">
                                        <label>Color</label>
                                        <input name="color" id="color" type="text" placeholder="Color" class="form-control color-input" maxlength="50" required="">
                                   </div>
                              </div>
                         </div>
                         <div class="row">
                              <div class="col-md-6">
                                   <label for="name">Imagen</label>
                                   <div id='img_container' style="text-align:center;">
                                        <button type="button" id="imagen_group_btn_delete" class="btn btn-danger btn-sm elim_btn" name="button" data-img="imagen_group" data-file="image" >
                                             <i class="fa fa-trash" aria-hidden="true"></i>
                                        </button>
                                        <input type="hidden" name="eliminar_imagen_group" value="0">

                                        <img style="max-height: 200px;" id="imagen_group" class="img-fluid img-thumbnail" src="https://www.dentallink.com.uy/components/com_virtuemart/assets/images/vmgeneral/no-image.jpg" alt="your image" title=''/>
                                   </div>
                                   <label class="input-file" style="margin-top: 10px;">
                                        Seleccionar imagen<input type="file" id="image" name="image" class="inputfile" data-prev='imagen_group'/>
                                   </label>
                              </div>
                              <div class="col-md-6">
                                   <label for="name">Imagen cabecera</label>
                                   <div id='img_container' style="text-align:center;">
                                        <button type="button" id="img_head_btn_delete" class="btn btn-danger btn-sm elim_btn" name="button" data-img="img_head" data-file="header_image" >
                                             <i class="fa fa-trash" aria-hidden="true"></i>
                                        </button>
                                        <input type="hidden" name="eliminar_img_head" value="0">
                                        <img  style="max-height: 200px;" id="img_head" class="img-fluid img-thumbnail" src="https://www.dentallink.com.uy/components/com_virtuemart/assets/images/vmgeneral/no-image.jpg" alt="your image" title=''/>
                                   </div>
                                   <label class="input-file" style="margin-top: 10px;">
                                        Seleccionar imagen<input type="file" name="header_image" class="inputfile" data-prev='img_head'/>
                                   </label>
                              </div>
                         </div>


                         <div class="clearfix"></div>
                    </div>
                    <div class="modal-footer">
                         <button type="button" class="btn btn-secundary" data-dismiss="modal">Cancelar</button>
                         <button type="submit" class="btn btn-success" id="btn-save" value="create">Guardar</button>
                    </div>
               </form>
          </div>
     </div>
</div>
@push('style')
<link rel="stylesheet" href="https://unpkg.com/huebee@1/dist/huebee.min.css">
<style>
input[type="file"]
{
     opacity:0;
     -moz-opacity: 0;
     /* IE 5-7 */
     filter: alpha(Opacity=0);
     /* Safari  */
     -khtml-opacity: 0;
     /* IE 8 */
     -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";

}
.elim_btn{
     position: absolute;
    margin-top: -15px;
    margin-left: -15px;
    background-color: #fff;
    padding: 10px;
}
.input-file
{
     margin: 0 auto;
     text-align: center;
     background-color: #3276b1;
     color: #fff !important;
     display: block;
     width: 180px;
     height: 36px;
     font-size: 17px;
     color: #fff;
     padding: 5px;
     font-weight: bold;
     border-radius: 10px;
}
</style>
@endpush


@push('scripts')
<script src="https://unpkg.com/huebee@1/dist/huebee.pkgd.min.js"></script>
<script>
var sin_imgen = 'https://www.dentallink.com.uy/components/com_virtuemart/assets/images/vmgeneral/no-image.jpg';

$(".inputfile").change(function(event) {
     readURL(this);
});

function readURL(input) {

     console.log(input);
     if (input.files && input.files[0]) {
          var reader = new FileReader();
          var filename = $(input).val();
          var prev = $(input).attr('data-prev');
          console.log(prev);
          filename = filename.substring(filename.lastIndexOf('\\')+1);
          reader.onload = function(e) {
               $('#'+prev).attr('src', e.target.result);
               $('#'+prev+'_btn_delete').show();

          }
          reader.readAsDataURL(input.files[0]);
     }
}

$(document).ready(function () {

     var elem = document.querySelector('.color-input');
     var hueb = new Huebee( elem, {
          // options
     });

     $.ajaxSetup({
          headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
     });
     /*  When group click add user button */
     var im_link = 'https://www.dentallink.com.uy/components/com_virtuemart/assets/images/vmgeneral/no-image.jpg';
     $('#create-new-user').click(function () {
          $('#color').val('#FFF');
          hueb.setColor('#FFF')
          $('#btn-save').val("create-user");
          $('#groupForm').trigger("reset");
          $('#groupCrudModal').html("Agregar Nuevo Grupo");
          $('#ajax-crud-modal').modal('show');
          $("#imagen_group").attr('src',im_link);
          $("#img_head").attr('src', im_link);

     });

     /* When click edit user */
     $('body').on('click', '#group-user', function () {
          var user_id = $(this).data('id');
          $.get('./admin/group/' + user_id +'/edit', function (data) {
               $('#groupCrudModal').html("Editar Grupo");
               $('#btn-save').val("edit-user");
               $('#ajax-crud-modal').modal('show');
               $('#group_id').val(data.id);
               $('#name').val(data.name);
               $('#color').val(data.color);
               hueb.setColor(data.color)
               if (data.image) {
                    $("#imagen_group").attr('src', data.image);
               }else {
                    $("#imagen_group").attr('src', im_link);
               }
               if (data.header_image) {
                    $("#img_head").attr('src', data.header_image);
               }else {
                    $("#img_head").attr('src', im_link);
               }
          })
     });
     //delete user login
     $('body').on('click', '.delete-user', function () {
          var user_id = $(this).data("id");
          confirm("Are You sure want to delete !");

          $.ajax({
               type: "DELETE",
               url: "{{ url('ajax-crud')}}"+'/'+user_id,
               success: function (data) {
                    $("#user_id_" + user_id).remove();
               },
               error: function (data) {
                    console.log('Error:', data);
               }
          });
     });
});

$('.elim_btn').click(function(){
          var img_change =$(this).data("img");
          var input_change = $(this).data("file");

          $('#'+img_change).attr('src', sin_imgen);
          $("[name='eliminar_"+input_change+"']").val(1);
          $("[name='"+input_change+"']").val('');
          $('#'+$(this).attr('id')).hide();

     });

if ($("#groupForm").length > 0) {
     $("#groupForm").validate({

          submitHandler: function(form) {

               var actionType = $('#btn-save').val();
               $('#btn-save').html('Sending..');

                var formData = new FormData($("#groupForm")[0]);


               $.ajax({
                    // data: $('#groupForm').serialize(),
                    data: formData,
                    url: "{{ route('admin.group.store') }}",
                    type: "POST",
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType: 'json',
                    success: function (data) {
                         location.reload();
                    },
                    error: function (data) {
                         console.log('Error:', data);
                         $('#btn-save').html('Save Changes');
                    }
               });
          }
     })
}


</script>


@endpush
