@extends('admin.layout')

@section('content')
<div class="row">
     <div class="col-md-12">
          <div class="card">
               <div class="card-header">
                    <h4 class="card-title">
                         Editar participante
                    </h4>
               </div>
               <div class="card-content ">
                    <form class="form" method="post" action="{{ route('admin.admin.update', $admin) }}">
                         {{ csrf_field() }} {{ method_field('PUT') }}
                         <div class="row">
                              <div class="col-md-12">
                                   @if($errors->any())
                                   <ul class="list-group">
                                        @foreach($errors->all() as $key => $error)
                                        <li class="list-group-item list-group-item-danger"> {{ $error }}</li>
                                        @endforeach
                                   </ul>
                                   @endif
                              </div>
                         </div>
                         <div class="row">
                              <div class="col-md-6">
                                   <div class="form-group">
                                        <label>Nombre</label>
                                        <input type="text" placeholder="Nombre" name="name" class="form-control"  value="{{ $admin->name }}">
                                   </div>
                              </div>
                              <div class="col-md-6">
                                   <div class="form-group">
                                        <label>Apellidos</label>
                                        <input type="text" name="surnames" placeholder="Apellidos" class="form-control" value="{{ $admin->surnames }}">
                                   </div>
                              </div>

                              <div class="col-md-6">
                                   <div class="form-group">
                                        <label>Correo</label>
                                        <input type="email"  value="{{  $admin->email }}" name="email" placeholder="Correo" class="form-control">
                                   </div>
                              </div>

                              <div class="col-md-6">
                                   <div class="form-group">
                                        <label>Estatus</label><br>
                                        <input
                                             name="status"
                                             @if($admin->status == 1) checked @endif
                                             type="checkbox"
                                             value="1"
                                             data-toggle="toggle"
                                             data-width="150"
                                             data-on="Activo"
                                             data-off="Inactivo"
                                             data-onstyle="success"
                                             data-offstyle="danger"
                                        >
                                   </div>
                              </div>
                         </div>
                         <hr>
                         <div class="row">
                              <div class="col-md-6">
                                   <div class="form-group">
                                        <label>Contraseña</label>
                                        <input type="password" name="password" placeholder="Contraseña" class="form-control">
                                   </div>
                              </div>
                              <div class="col-md-6">
                                   <div class="form-group">
                                        <label>Confirmar Contraseña</label>
                                        <input type="password" name="password_confirmation" placeholder="Confirmar contraseña" class="form-control">
                                   </div>
                              </div>
                         </div>
                         <hr>

                         <a  href="{{ url('admin/admin') }}" class="btn btn-fill btn-wd btn-default">
                              <i class="fa fa-arrow-left" aria-hidden="true"></i>
                              Regresar
                         </a>
                         <button type="submit" class="btn btn-fill btn-success btn-wd pull-right">
                              <i class="fa fa-floppy-o" aria-hidden="true"></i>
                              Editar administrador
                         </button>
                    </form>
               </div>
          </div>
     </div>
</div>
@stop
@push('scripts')
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
@endpush

@push('style')
<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">

<style>
span.badge.badge-secondary{
     width: 100%;
    padding: 10px;
    background-color: #f3f2ee;
    color: #6a615b;
    border-radius: 5px;
    border: 1px solid #ddd;
}

</style>
@endpush
