@extends('admin.layout')

@section('content')
<div class="row">
     <div class="col-md-12">
          <div class="card">
               <div class="card-header">
                    <h4 class="card-title">
                         Nuevo administrador
                    </h4>
               </div>
               <div class="card-content ">
                    <form class="form" method="post" action="{{ route('admin.admin.store') }}">
                         {{ csrf_field() }}
                         <div class="row">
                              <div class="col-md-12">
                                   @if($errors->any())
                                   <ul class="list-group">
                                        @foreach($errors->all() as $key => $error)
                                        <li class="list-group-item list-group-item-danger"> {{ $error }}</li>
                                        @endforeach
                                   </ul>
                                   @endif
                              </div>
                         </div>
                         <div class="row">
                              <div class="col-md-6">
                                   <div class="form-group">
                                        <label>Nombre</label>
                                        <input type="text" placeholder="Nombre" name="name" class="form-control" value="{{ old('name') }}">
                                   </div>
                              </div>
                              <div class="col-md-6">
                                   <div class="form-group">
                                        <label>Apellidos</label>
                                        <input type="text" name="surnames" placeholder="Apellidos" class="form-control" value="{{ old('surnames') }}">
                                   </div>
                              </div>
                              <div class="col-md-6">
                                   <div class="form-group">
                                        <label>Correo</label>
                                        <input type="email"  value="{{ old('email') }}" name="email" placeholder="Correo" class="form-control">
                                   </div>
                              </div>
                         </div>
                         <hr>
                         <div class="row">
                              <div class="col-md-6">
                                   <div class="form-group">
                                        <label>Contraseña</label>
                                        <input type="password" name="password" placeholder="Contraseña" class="form-control">
                                   </div>
                              </div>
                              <div class="col-md-6">
                                   <div class="form-group">
                                        <label>Confirmar Contraseña</label>
                                        <input type="password" name="password_confirmation" placeholder="Confirmar contraseña" class="form-control">
                                   </div>
                              </div>
                         </div>
                         <hr>

                         <a  href="{{ url('admin/admin') }}" class="btn btn-fill btn-wd btn-default">
                              <i class="fa fa-arrow-left" aria-hidden="true"></i>
                              Regresar
                         </a>
                         <button type="submit" class="btn btn-fill btn-success btn-wd pull-right">
                              <i class="fa fa-floppy-o" aria-hidden="true"></i>
                              Registrar administrador
                         </button>
                    </form>
               </div>
          </div>
     </div>
</div>
@stop
