@extends('admin.layout')

@section('content')

<div class="row">
     <div class="col-xs-6">
          <a href="{{ url('/admin') }}">
               <h4 style="margin-top:3px;"><span class="label label-success">
                    <i class="fa fa-arrow-left"></i> Atras</span></h4>
          </a>
     </div>
     <div class="col-xs-6" style="text-align: right;">
          <ol class="breadcrumb">
               <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Inicio</a></li>
               <li class="breadcrumb-item active" aria-current="page">Lista de administradores</li>
          </ol>
     </div>
</div>

<div class="row">
     <div class="col-md-12">
          <div class="card">
               <div class="card-header">
                    <a href="{{ route('admin.admin.create') }}" class="btn btn-wd btn-success" style="float:right; z-index:9999;">
                         <span class="btn-label">
                              <i class="fa fa-plus"></i>
                         </span>
                         Agregar un nuevo administrador
                    </a>
                    <h4 class="card-title">Lista de administradores</h4>
                    <p class="category">Administradores registrados</p>

               </div>
               <div class="card-content table-responsive table-full-width">
                    <table class="table">
                         <thead>
                              <tr>
                                   <th class="text-center">#</th>
                                   <th>Nombre</th>
                                   <th class="text-center">Estatus</th>
                                   <th class="text-center">Actions</th>
                              </tr>
                         </thead>
                         <tbody>
                              @forelse($admins as $key => $admin)
                              <tr>
                                   <td class="text-center">{{ $admin->id }}</td>
                                   <td>{{ $admin->name }} {{ $admin->surnames }}</td>
                                   <td class="text-center">
                                        @if($admin->status == 1)
                                             <span class="label label-success">Activo</span>
                                        @else
                                             <span class="label label-danger">Inactivo</span>
                                        @endif
                                   </td>
                                   <td class="td-actions text-center">
                                        <a href="{{ route('admin.admin.edit', $admin) }}" rel="tooltip" title="" class="btn btn-success  btn-simple btn-xs" data-original-title="Editar">
                                             <i class="ti-pencil-alt"></i>
                                        </a>
                                        <form method="POST"
                                             action="{{ route('admin.admin.destroy', $admin) }}"
                                             style="display:inline">
                                             {{ csrf_field() }} {{ method_field('DELETE') }}
                                             <button class="btn btn-xs btn-simple btn-danger"
                                                  onclick="return confirm('¿ Estás seguro de querer eliminar este administrador ?')"
                                              >
                                              <i class="fa fa-trash"></i>
                                             </button>
                                        </form>
                              </td>
                         </tr>
                         @empty
                         <tr>
                              <td colspan="5" class="text-center">Sin registros</td>
                         </tr>
                         @endforelse
                    </tbody>
               </table>
          </div>
     </div>
</div>
</div>
@stop
