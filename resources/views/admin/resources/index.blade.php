@extends('admin.layout')

@section('content')

<div class="row">
     <div class="col-xs-6">
          <a href="{{ url('/admin') }}">
               <h4 style="margin-top:3px;"><span class="label label-success">
                    <i class="fa fa-arrow-left"></i> Atras</span></h4>
          </a>
     </div>
     <div class="col-xs-6" style="text-align: right;">
          <ol class="breadcrumb">
               <li class="breadcrumb-item"><a href="{{ url('/admin') }}">Inicio</a></li>
               <li class="breadcrumb-item active" aria-current="page">Lista de temas -> {{ $group->name }}</li>
          </ol>
     </div>
</div>

<div class="row">
     <div class="col-md-12">
          <div class="card">
               <div class="card-header">
                    <a href="{{ url('admin/'.$group->id.'/resource/create') }}" class="btn btn-wd btn-success" style="float:right; z-index:9999;">
                         <span class="btn-label">
                              <i class="fa fa-plus"></i>
                         </span>
                         Agregar un nuevo tema
                    </a>
                    <h4 class="card-title">Temario</h4>
                    <p class="category"> {{ $group->name }} \ Temas disponibles</p>
               </div>
               <div class="card-content table-responsive table-full-width">
                    <table class="table">
                         <thead>
                              <tr>
                                   <th class="text-center">#</th>
                                   <th class="text-center">Tema</th>
                                   <th class="text-center">Tipo de recurso</th>
                                   <th class="text-center">Fecha de publicación</th>
                                   <th class="text-center">Activo</th>
                                   <th class="text-center">Actions</th>
                              </tr>
                         </thead>
                         <tbody id="example1">
                              @forelse ($resource as $resource)
                              <tr id="{{ $resource->id }}">
                                   <td class="text-center">{{ $resource->id }}</td>
                                   <td class="text-center">{{ $resource->name }}</td>
                                   <td class="text-center">{{ $resource->resource_type->name }}</td>
                                   @if($resource->publication_date)
                                        <td class="text-center"> {{ \Carbon\Carbon::parse($resource->publication_date)->diffForHumans() }}</td>
                                   @else
                                        <td class="text-center">Sin fecha</td>
                                   @endif
                                   <td class="text-center">
                                        <input
                                             class="status_resource"
                                             type="checkbox"
                                             data-resource="{{ $resource->id }}"
                                             data-toggle="toggle"
                                             data-size="sm"
                                             @if( $resource->status == 1) checked @endif
                                        >

                                   </td>
                                   <td class="td-actions text-center">
                                        <a href="{{  route('resource.edit',  ['group_id' => $group->id, 'resource' => $resource ] ) }}" rel="tooltip" title="" class="btn btn-success btn-simple btn-xs" data-original-title="Editar">
                                             <i class="ti-pencil-alt"></i>
                                        </a>

                                        <form method="POST"
                                             action="{{ route('resource.destroy', [ 'group_id' => $group->id, 'resource' => $resource ]) }}"
                                             style="display:inline">
                                             {{ csrf_field() }} {{ method_field('DELETE') }}
                                             <button class="btn btn-xs btn-simple btn-danger"
                                                  onclick="return confirm('¿ Estás seguro de querer eliminar este recurso ?')"
                                             >
                                             <i class="fa fa-trash"></i>
                                             </button>
                                        </form>
                                   </td>
                              </tr>

                              @empty
                              <tr>
                                   <td colspan="5">Sin recursos</td>
                              </tr>
                              @endforelse


                         </tbody>
                    </table>
               </div>
          </div>
     </div>
</div>
@stop
@push('scripts')
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
<script src="{!! asset('js/Sortable.js') !!}" charset="utf-8"></script>
<script src="{!! asset('js/st/prettify/prettify.js') !!}" charset="utf-8"></script>
<script src="{!! asset('js/st/prettify/run_prettify.js') !!}" charset="utf-8"></script>
<script>
     $.ajaxSetup({
          headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
     });

     var example1 = document.getElementById('example1');
     new Sortable(example1, {
     	animation: 150,
     	ghostClass: 'blue-background-class',
          onUpdate: function(evt) {
          console.log(evt);
          var order= $("#example1 tr").map(function() {
               return this.id;
          }).get();
            f='order='+order;
            $.ajax({
                 type: "POST",
                 data : f,
                 cache: false,
                 url: " {{ route('admin.order_resource')}}",
                 success: function(data){
                    //success
                 }
            });
	    },
     });

</script>

<script>
$('.status_resource').change(function() {
     var status = $(this).prop('checked');
     var resource_id = $(this).data("resource");
     var _token = $("input[name='_token']").val();

     $.ajax({
          data: {  _token:_token, status : status, resource_id: resource_id },
          url: "{{ route('admin.activate_resource') }}",
          type: "POST",
          dataType: 'json',
          beforeSend : function( xhr ) {
          	$('.status_resource').bootstrapToggle('disable')
          },
          success: function (data) {
               var men = data.status == 1 ? 'Activado' : 'Desactivado' ;
               Swal.fire(
                 'Exito',
                  'Se '+men+' correctamente',
                 'success'
               )
          },
          error: function (data) {
               console.log('Error:', data);
               $('#btn-save').html('Save Changes');
          },
          complete: function() {
               $('.status_resource').bootstrapToggle('enable');
          }

     });

})
</script>
@endpush

@push('style')
<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
@endpush
