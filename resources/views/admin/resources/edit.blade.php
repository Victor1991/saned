@extends('admin.layout')

@section('content')
<div class="row">
     <div class="col-md-12">
          <div class="card">
               <div class="card-header">
                    <h4 class="card-title">
                         {{ $group->name }} \ Editar tema
                    </h4>
               </div>
               <div class="card-content ">
                    <form method="post" enctype="multipart/form-data" action="{{ route('resource.update', ['group_id' => $group->id, 'resource' =>  $resource ] ) }}">
                         {{ csrf_field() }} {{ method_field('PUT') }}

                         <div class="row">
                              <div class="col-md-12">
                                   @if($errors->any())
                                   <ul class="list-group">
                                        @foreach($errors->all() as $key => $error)
                                        <li class="list-group-item list-group-item-danger"> {{ $error }}</li>
                                        @endforeach
                                   </ul>
                                   @endif
                              </div>
                         </div>
                         <div class="row">
                              <div class="col-md-6">
                                   <div class="form-group">
                                        <label>Nombre</label>
                                        <input type="text" placeholder="Nombre" name="nombre" class="form-control" value="{{ $resource->name }}">
                                   </div>
                              </div>
                              <div class="col-md-6">
                                   <div class="form-group">
                                        <label>Tipo de recurso</label>
                                        <select class="form-control" name="resource_type_id" id="resource_type_id" onchange="cambio_archivo()">
                                             @foreach ($resourcetype as $key => $type)
                                             <option
                                                  @if( $resource->resource_type_id == $type->id ) selected @endif
                                                  value="{{ $type->id }}"
                                             >
                                             {{ $type->name }}
                                             </option>
                                             @endforeach
                                        </select>
                                   </div>
                              </div>
                              <div class="col-md-12" id="div_archivo">
                                   <label for="name">Archivo</label>
                                   <div class="input-group">
                                        <label class="input-group-btn">
                                             <span class="btn btn-info">
                                                  Archivo &hellip; <input type="file" name="input_file" id="input_file" style="display: none;">
                                             </span>
                                        </label>
                                        <input type="text" class="form-control" readonly>
                                   </div>
                                   <hr>
                                   <label>Archivo actual</label>
                                   <div class="row">
                                        <div class="col-md-12">
                                             <div class="col-md-12  text-center"  style="height:300px; border-radiborder-radius: 5px; borde:1px solid #eee; background-color:#ddd; overflow-y:auto;">
                                        <?php if ( $resource->file): ?>
                                             <?php if($resource->resource_type_id == '1'): ?>
                                                <iframe src="{{ $resource->file }}" width="100%" height="500px" style="margin: 0 auto;"></iframe>
                                             <?php elseif($resource->resource_type_id == '2'): ?>
                                                  <img src="{{ $resource->file }}" alt="" style="margin-top: 15%; margin: 0 auto;">
                                             <?php elseif($resource->resource_type_id == '3'): ?>
                                                  <video src="{{ $resource->file }}" controls style="width:500px; margin: 0 auto;" >
                                                  </video>
                                             <?php endif; ?>
                                        <?php endif; ?>

                                             </div>
                                        </div>
                                   </div>
                                   <hr>
                              </div>

                              <div class="col-md-12" id="div_link">
                                   <div class="form-group">
                                        <label>Link del recurso</label>
                                        <input type="text" placeholder="link" name="url" value="{{ $resource->url }}" class="form-control">
                                   </div>
                              </div>
                              <div class="col-md-6">
                                   <div class="form-group">
                                        <label>Autor del recurso</label>
                                        <input type="text" placeholder="Autor" value="{{ $resource->author }}" name="author" class="form-control">
                                   </div>
                              </div>
                              <div class="col-md-6">
                                   <div class="form-group">
                                        <label>Especialidad del autor</label>
                                        <input type="text" placeholder="Especialidad" value="{{ $resource->author_specialty }}" name="author_specialty" class="form-control">
                                   </div>
                              </div>
                              <div class="col-md-12">
                                   <div class="form-group">
                                        <label>Descripción</label><br>
                                        <textarea name="description" class="form-control" rows="8" cols="80" name="description"> {{ $resource->description }}</textarea>
                                   </div>
                              </div>
                         </div>
                         <hr>

                         <a  href="{{ url('admin/'.$group->id.'/resource') }}" class="btn btn-fill btn-wd btn-default">
                              <i class="fa fa-arrow-left" aria-hidden="true"></i>
                              Regresar
                         </a>
                         <button type="submit" class="btn btn-fill btn-success btn-wd pull-right">
                              <i class="fa fa-floppy-o" aria-hidden="true"></i>
                              Publicar recurso
                         </button>
                    </form>
               </div>
          </div>
     </div>
</div>
@stop
@push('style')
<style>

video { display:block; width:100%; border:none; border: 1px solid #eee; border-radius: 5px; background-color: #ddd};
embed{ display:block; width:100%; border:none; border: 1px solid #eee;  border-radius: 5px; background-color: #ddd };

.bord{
     border: 1px solid #e8e7e3 !important;
     border-radius: 5px;
     padding: 10px;
}

.btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}


</style>
@endpush


@push('scripts')
<script>
$(function() {

  // We can attach the `fileselect` event to all file inputs on the page
  $(document).on('change', ':file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
  });

  // We can watch for our custom `fileselect` event like this
  $(document).ready( function() {
     cambio_archivo();

      $(':file').on('fileselect', function(event, numFiles, label) {

          var input = $(this).parents('.input-group').find(':text'),
              log = numFiles > 1 ? numFiles + ' files selected' : label;

          if( input.length ) {
              input.val(log);
          } else {
              if( log ) alert(log);
          }

      });
  });

});


function cambio_archivo() {
     $('#div_archivo').hide();
     $('#div_link').hide();
     $('#text_file').val('');
     $('#input_file').val('');
     $('#url').val('');

     var resource_type_id =  $('#resource_type_id').val();
     switch (resource_type_id) {
          case '1':
               $('#div_archivo').show();
               $('#input_file').attr("accept", "application/pdf");
          break;
          case '2':
               $('#div_archivo').show();
               $('#input_file').attr("accept", "image/*");
          break;
          case '3':
               $('#div_archivo').show();
               $('#input_file').attr("accept", "video/mp4");
          break;
          case '4':
               $('#div_link').show();
          break;


     }
}

</script>

@endpush
