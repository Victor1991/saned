@extends('admin.layout')

@section('content')
<div class="row">
     <div class="col-md-12">
          <div class="card">
               <div class="card-content">
                    <div class="row">
                         <div class="col-md-5 text-center">
                              <h1>Bienvenid@</h1>
                         </div>
                         <div class="col-md-7 text-center" style="margin-top:30px;">
                              <a href="{{ route('admin.my_account') }}" type="button" class="btn btn-wd btn-info btn-lg">
                                   <span class="btn-label">
                                        <i class="fa fa-user" aria-hidden="true"></i>
                                   </span>
                                   Mi cuenta
                              </a>
                              <a  href="{{ url('/logout') }}" class="btn btn-wd btn-default btn-lg mb-4">
                                   <span class="btn-label">
                                        <i class="ti-power-off"></i>
                                   </span>
                                   Salir
                              </a>
                         </div>
                    </div>
               </div>
               <div class="card-footer">
                    <hr>
                    <div class="row">
                         <div class="col-md-6">
                              <div class="row">

                                   <div class="col-md-10 col-sm-offset-1">
                                        <div class="numbers" style="text-align:left;">
                                             <span style="font-size: 50px;">  <i class="fa fa-users " aria-hidden="true" style="color: #a5bbce"></i> {{ $users->count()}}</span>
                                             <p>VISITANTES REGISTRADOS</p>
                                        </div>
                                   </div>
                              </div>
                              <hr>
                              <div class="row">
                                   <div class="col-md-10 col-sm-offset-1">
                                        <a href="{{ url('admin/visitor') }}" class="btn btn-wd btn-block btn-primary btn-lg">
                                             <span class="btn-label">
                                                  <i class="fa fa-list"></i>
                                             </span>
                                             Ver lista de visitantes
                                        </a>
                                        <br>
                                        <a href="{{ route('admin.visitor.create') }}" class="btn btn-wd btn-block btn-success btn-lg">
                                             <span class="btn-label">
                                                  <i class="fa fa-plus"></i>
                                             </span>
                                             Agregar un visitante
                                        </a>
                                   </div>
                              </div>
                         </div>
                         <div class="col-md-6">
                              <div class="row">

                                   <div class="col-md-10 col-sm-offset-1">
                                        <div class="numbers" style="text-align:left;">

                                             <span style="font-size: 50px;"><i class="fa fa-archive " aria-hidden="true" style="color: #a5bbce"></i>
                                                  {{ $groups->count() }}</span>
                                             <p>GRUPO DE RECURSOS</p>
                                        </div>
                                   </div>
                              </div>
                              <hr>
                              <div class="row">
                                   <div class="col-md-10 col-sm-offset-1">
                                        <div class="grup_recursos">
                                             <ul id="example1" class="list-group">
                                                  @forelse ($groups as $group)

                                                       <li id="{{$group->id}}"  class="list-group-item d-flex justify-content-between align-items-center">
                                                            <a href="{{ url('admin/'.$group->id.'/resource') }}">
                                                                 {{ $group->name }}

                                                            </a>
                                                            <!-- <a href="javascript:void(0)" style="padding: 0px 5px 10px 10px; margin-top: -2px;" id="group-user" data-id="{{ $group->id }}" class="btn btn-danger pull-right btn-simple" >
                                                                 <i class="ti-trash"></i>
                                                            </a> -->
                                                            <form method="POST"
                                                                 action="{{ route('admin.group.destroy', $group) }}"
                                                                 style="display:inline">
                                                                 {{ csrf_field() }} {{ method_field('DELETE') }}
                                                                 <button class="btn btn-xs btn-simple btn-danger"
                                                                      onclick="return confirm('¿ Estás seguro de querer eliminar este grupo ?')"
                                                                 >
                                                                 <i class="fa fa-trash"></i>
                                                                 </button>
                                                            </form>
                                                            <a href="javascript:void(0)" style="padding: 0px 5px 10px 10px; margin-top: 1px;" id="group-user" data-id="{{ $group->id }}" class="btn btn-success pull-right btn-simple" >
                                                                 <i class="ti-pencil-alt"></i>
                                                            </a>

                                                            <span class="badge badge-primary badge-pill">{{ $group->resources->count() }}</span>
                                                       </li>

                                                  @empty
                                                  <p>No hay grupos.</p>
                                                  @endforelse
                                             </ul>
                                        </div>
                                   </div>
                                   <div class="col-md-10 col-sm-offset-1">
                                        <a href="javascript:void(0)" id="create-new-user" class="btn btn-wd btn-block btn-success btn-lg">
                                             <span class="btn-label">
                                                  <i class="fa fa-plus"></i>
                                             </span>
                                             Agregar un grupo
                                        </a>
                                   </div>
                              </div>
                         </div>
                    </div>
                    <hr>
                    <div class="row">
                         <div class="col-md-6">
                              <div class="row">

                                   <div class="col-md-10 col-sm-offset-1">
                                        <div class="numbers" style="text-align:left;">
                                             <span style="font-size: 50px;"><i class="fa fa-lock " aria-hidden="true" style="color: #a5bbce"></i> {{ $admin->count()}}</span>
                                             <p>ADMINISTRADORES REGISTRADOS</p>
                                        </div>
                                        <hr>
                                        <div class="row">
                                             <div class="col-md-10 col-sm-offset-1">
                                                  <a href="{{ url('admin/admin') }}" class="btn btn-wd btn-block btn-primary btn-lg">
                                                       <span class="btn-label">
                                                            <i class="fa fa-list"></i>
                                                       </span>
                                                       Ver lista de administradores
                                                  </a>
                                                  <br>
                                                  <a href="{{ route('admin.admin.create') }}" class="btn btn-wd btn-block btn-success btn-lg">
                                                       <span class="btn-label">
                                                            <i class="fa fa-plus"></i>
                                                       </span>
                                                       Agregar un administrador
                                                  </a>
                                             </div>
                                        </div>
                                   </div>
                              </div>
                         </div>
          </div>
     </div>
</div>
@stop
@include('admin.resourcegroup.ajax_crud_modal')
@push('scripts')
<script src="{!! asset('js/Sortable.js') !!}" charset="utf-8"></script>
<script src="{!! asset('js/st/prettify/prettify.js') !!}" charset="utf-8"></script>
<script src="{!! asset('js/st/prettify/run_prettify.js') !!}" charset="utf-8"></script>
<script>
     var example1 = document.getElementById('example1');
     new Sortable(example1, {
     	animation: 150,
     	ghostClass: 'blue-background-class',
          onUpdate: function(evt) {
          console.log(evt);
          var order= $("#example1 li").map(function() {
               return this.id;
          }).get();
            f='order='+order;
            $.ajax({
                 type: "POST",
                 data : f,
                 cache: false,
                 url: " {{ route('admin.order_group')}}",
                 success: function(data){
                    //success
                 }
            });
	    },
     });

</script>

@endpush
