<!doctype html>
<html lang="en">
<head>
     <meta charset="utf-8" />
     <link rel="apple-touch-icon" sizes="76x76" href="{!! asset('theme/img/apple-icon.png') !!}">
     <link rel="icon" type="image/png" sizes="96x96" href="{!! asset('theme/img/favicon.png') !!}">
     <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
     <meta name="csrf-token" content="{{ csrf_token() }}">
     <title>{{ config('app.name') }}</title>


     <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
     <meta name="viewport" content="width=device-width" />


     <!-- Bootstrap core CSS     -->
     <link href="{!! asset('theme/css/bootstrap.min.css') !!}" rel="stylesheet" />

     <!--  Paper Dashboard core CSS    -->
     <link href="{!! asset('theme/css/paper-dashboard.css') !!}" rel="stylesheet"/>


     <!--  CSS for Demo Purpose, don't include it in your project     -->
     <link href="{!! asset('theme/css/demo.css') !!}" rel="stylesheet" />


     <!--  Fonts and icons     -->
     <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
     <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
     <link href="{!! asset('theme/css/themify-icons.css') !!}" rel="stylesheet">

     @stack('style')


     <div class="main-panel">
          <nav class="navbar navbar-default">
               <div class="container-fluid">

                    <div class="navbar-header">
                         <button type="button" class="navbar-toggle">
                              <span class="sr-only">Toggle navigation</span>
                              <span class="icon-bar bar1"></span>
                              <span class="icon-bar bar2"></span>
                              <span class="icon-bar bar3"></span>
                         </button>
                         <a class="navbar-brand" href="/admin">
                              <img src="{{URL::asset('/img/logo.png')}}" alt="SANED" style="width:100px;">
                         </a>
                    </div>
                    <div class="collapse navbar-collapse">

                         <ul class="nav navbar-nav navbar-right">
                              <li>
                                   <a href="{{ url('/admin') }}" class="btn-rotate">
                                        <i class="ti-home"></i>
                                        Inicio
                                   </a>
                              </li>
                              <li>

                                   <a href="{{ url('/logout') }}" class="btn-rotate">
                                        <i class="ti-power-off"></i>
                                        Salir
                                   </a>
                              </li>
                         </ul>
                    </div>
               </div>
          </nav>
          <div class="content">
               <div class="container">
                    <div class="container-fluid">

                         @include('partials.flash-message')

                         @yield('content')
                    </div>
               </div>
          </div>

          <footer class="footer">
               <div class="container-fluid">

                    <div class="copyright pull-right">
                    &copy; <script>document.write(new Date().getFullYear())</script> -   Elaborado por grupo saned
                    </div>
               </div>
          </footer>
     </div>
</div>

</body>

<!--   Core JS Files. Extra: TouchPunch for touch library inside jquery-ui.min.js') !!}   -->
<script src="{!! asset('theme/js/jquery.min.js') !!}" type="text/javascript"></script>
<script src="{!! asset('theme/js/jquery-ui.min.js') !!}" type="text/javascript"></script>
<script src="{!! asset('theme/js/perfect-scrollbar.min.js') !!}" type="text/javascript"></script>
<script src="{!! asset('theme/js/bootstrap.min.js') !!}" type="text/javascript"></script>

<!--  Forms Validations Plugin -->
<script src="{!! asset('theme/js/jquery.validate.min.js') !!}"></script>

<!-- Promise Library for SweetAlert2 working on IE -->
<script src="{!! asset('theme/js/es6-promise-auto.min.js') !!}"></script>

<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
<script src="{!! asset('theme/js/moment.min.js') !!}"></script>

<!--  Date Time Picker Plugin is included in this js file -->
<script src="{!! asset('theme/js/bootstrap-datetimepicker.js') !!}"></script>

<!--  Select Picker Plugin -->
<script src="{!! asset('theme/js/bootstrap-selectpicker.js') !!}"></script>

<!--  Switch and Tags Input Plugins -->
<script src="{!! asset('theme/js/bootstrap-switch-tags.js') !!}"></script>

<!-- Circle Percentage-chart -->
<script src="{!! asset('theme/js/jquery.easypiechart.min.js') !!}"></script>

<!--  Charts Plugin -->
<script src="{!! asset('theme/js/chartist.min.js') !!}"></script>

<!--  Notifications Plugin    -->
<script src="{!! asset('theme/js/bootstrap-notify.js') !!}"></script>

<!-- Sweet Alert 2 plugin -->
<script src="{!! asset('theme/js/sweetalert2.js') !!}"></script>

<!-- Vector Map plugin -->
<script src="{!! asset('theme/js/jquery-jvectormap.js') !!}"></script>

<!-- Wizard Plugin    -->
<script src="{!! asset('theme/js/jquery.bootstrap.wizard.min.js') !!}"></script>

<!--  Bootstrap Table Plugin    -->
<script src="{!! asset('theme/js/bootstrap-table.js') !!}"></script>

<!--  Plugin for DataTables.net  -->
<script src="{!! asset('theme/js/jquery.datatables.js') !!}"></script>

<!--  Full Calendar Plugin    -->
<script src="{!! asset('theme/js/fullcalendar.min.js') !!}"></script>

<!-- Paper Dashboard PRO Core javascript and methods for Demo purpose -->
<script src="{!! asset('theme/js/paper-dashboard.js') !!}"></script>

<!--   Sharrre Library    -->
<script src="{!! asset('theme/js/jquery.sharrre.js') !!}"></script>

<!-- Paper Dashboard PRO DEMO methods, don't include it in your project! -->
<script src="{!! asset('theme/js/demo.js') !!}"></script>

<script>

//Script Eliminar
$(".eliminar").submit(function(e){
     e.preventDefault();

     Swal.fire({
          title: '¿ Desea eliminar ?',
          text: "¡No podrás revertir esto!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si, eliminar',
          cancelButtonText: 'Cancelar'
     }).then((result) => {
          if (result.value) {
               $(".eliminar").off("submit").submit();
          }else {
               return false;
          }
     })
});
</script>

@stack('scripts')

</html>
