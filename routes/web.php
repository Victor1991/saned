<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



// Página principal
Route::get('/', 'PageController@home')->name('visitor.dashboard.index');

// Autenticación Visitor
Route::auth();

// Obtener municipios
     Route::get('municipios', 'PageController@municipios')->name('municipios');

// Grupo Visitor
Route::group([
     'prefix' => 'visitor',
     'namespace' => 'Visitor',
     'middleware' => 'auth'],
     function(){
          Route::get('/', 'DashboardController@index')->name('visitor.dashboard');
          Route::get('/resource_group/{group_id}', 'ResourceController@index')->name('visitor.resource_group');
          Route::get('/see_resource/{resource_id}', 'ResourceController@see_resource')->name('visitor.see_resource');

          Route::get('my_account', 'UserController@my_account')->name('visitor.my_account');
          Route::put('my_account_update', 'UserController@my_account_update')->name('visitor.my_account_update');
});

// Login Admin
Route::get('/admin/login', 'Auth\LoginAdminController@showLoginForm')->name('admin.login');
Route::post('/admin/login', 'Auth\LoginAdminController@login');


// Grupo Admin
Route::group([
     'prefix' => 'admin',
     'namespace' => 'Admin',
     'middleware' => 'is_admin'],
      function(){
          Route::get('/', 'DashboardController@index')->name('dashboard');
          Route::resource('visitor', 'UserController', ['as' => 'admin']);
          Route::resource('group', 'ResourceGroupController', ['as' => 'admin']);
          Route::resource('admin', 'AdminController', ['as' => 'admin']);

          Route::post('activate_resource', 'ResourceController@activate_resource')->name('admin.activate_resource');

          Route::get('my_account', 'UserController@my_account')->name('admin.my_account');
          Route::put('my_account_update', 'UserController@my_account_update')->name('admin.my_account_update');


          Route::group(['prefix' => '{group_id}'], function($group_id){
               Route::resource('resource', 'ResourceController');
          });

          Route::post('order_group', 'ResourceGroupController@order_group')->name('admin.order_group');

          Route::post('order_resource', 'ResourceController@order_resource')->name('admin.order_resource');

});

// Salir de sesión
Route::get('logout', 'Auth\LoginController@logout')->name('logout');
