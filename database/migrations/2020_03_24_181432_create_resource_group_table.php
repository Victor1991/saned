<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResourceGroupTable extends Migration
{
     /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
          Schema::create('resource_groups', function (Blueprint $table) {
               $table->increments('id');
               $table->string('name');
               $table->string('color')->nullable();
               $table->string('image')->nullable();
               $table->string('header_image')->nullable();
               $table->integer('user_id');
               $table->timestamps();
          });
     }

     /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
          Schema::dropIfExists('resource_groups');
     }
}
