<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModelResourceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resources', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('resource_type_id');
            $table->string('file')->nullable();
            $table->string('url')->nullable();
            $table->string('author')->nullable();
            $table->datetime('publication_date')->nullable();
            $table->integer('status')->default(0);
            $table->longText('description')->nullable();
            $table->string('author_specialty')->nullable();
            $table->integer('resource_group_id');
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resources');
    }
}
