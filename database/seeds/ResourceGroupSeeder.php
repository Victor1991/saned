<?php

use Illuminate\Database\Seeder;
use App\ResourceGroup;

class ResourceGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         ResourceGroup::truncate();


         $grupo = new ResourceGroup;
         $grupo->name = 'Suma Web';
         $grupo->color = '#FFF';
         $grupo->image = '';
         $grupo->header_image = '';
         $grupo->user_id = '1';
         $grupo->save();
    }
}
