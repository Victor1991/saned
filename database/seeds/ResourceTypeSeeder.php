<?php

use Illuminate\Database\Seeder;
use App\ResourceType;

class ResourceTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       ResourceType::truncate();

       $types = array('pdf', 'imagen', 'video', 'link');

       foreach ($types as $key => $type) {
            $resource = new ResourceType;
            $resource->name = $type;
            $resource->save();
       }


    }
}
