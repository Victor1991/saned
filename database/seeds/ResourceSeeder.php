<?php

use Illuminate\Database\Seeder;
use App\Resource;

class ResourceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         Resource::truncate();

         $resource = new Resource;
         $resource->name = 'nombre';
         $resource->resource_type_id = '1';
         $resource->file = '';
         $resource->url = '';
         $resource->author = 'Victor Hugo';
         $resource->author_specialty = 'Desarrollo';
         $resource->description = 'Ejemplo';
         $resource->resource_group_id = 1;
         $resource->user_id = '1';
         $resource->save();

    }
}
