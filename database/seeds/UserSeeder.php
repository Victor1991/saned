<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
     /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
     {
          User::truncate();

          // Crear Usuario
          $admin = new User;
          $admin->name = 'Victor Hugo';
          $admin->surnames = 'Uribe Hdz';
          $admin->email = 'vic.hug.urib@gmail.com';
          $admin->password = '12345678';
          $admin->is_admin = '1';
          $admin->save();
     }
}
