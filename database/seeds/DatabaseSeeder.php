<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UserSeeder::class);
         $this->call(SpecialtySeeder::class);
         $this->call(ResourceGroupSeeder::class);
         $this->call(ResourceSeeder::class);
         $this->call(ResourceTypeSeeder::class);
    }
}
