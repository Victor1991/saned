<?php

use Illuminate\Database\Seeder;
use App\Specialty;

class SpecialtySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         Specialty::truncate();

         // Crear Crear specialidad
          $Specialty = new Specialty;
          $Specialty->name = 'Ginecólogo General';
          $Specialty->save();

          // Crear Crear specialidad
           $Specialty = new Specialty;
           $Specialty->name = 'Ginecólogo con subespecialidad en climaterio';
           $Specialty->save();

           // Crear Crear specialidad
            $Specialty = new Specialty;
            $Specialty->name = 'Ginecólogo con subespecialidad en colposcopía';
            $Specialty->save();

            // Crear Crear specialidad
           $Specialty = new Specialty;
           $Specialty->name = 'Ginecólogo con otra subespecialidad';
           $Specialty->save();

           // Crear Crear specialidad
          $Specialty = new Specialty;
          $Specialty->name = 'Médico General ';
          $Specialty->save();

          // Crear Crear specialidad
         $Specialty = new Specialty;
         $Specialty->name = 'Médico General con subespecialidad en climaterio';
         $Specialty->save();

         // Crear Crear specialidad
         $Specialty = new Specialty;
         $Specialty->name = 'Médico General con subespecialidad en colposcopía';
         $Specialty->save();

         // Crear Crear specialidad
        $Specialty = new Specialty;
        $Specialty->name = 'Médico General con otra subespecialidad';
        $Specialty->save();

    }
}
